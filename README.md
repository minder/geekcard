# README #

Geekcard jest systemem obsługi klientów dla sklepów hobbystycznych.
Zawiera następujące moduły:

1. Karta klienta
2. Liga
3. Wypożyczalnia
4. Kolekcjoner

## LICENCJA ##

Niniejsze oprogramowanie jest udostępnione na licencji [GNU Affero GPL v3](http://www.gnu.org/licenses/agpl-3.0.html)
