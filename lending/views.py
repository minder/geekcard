# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, FormView, DetailView, View
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.forms.models import inlineformset_factory
from django.core.exceptions import PermissionDenied
from django.views.generic.base import TemplateResponseMixin, TemplateView
from .models import Item, GameToLend, LendIt, Cart, CartItem
from .forms import LendItInlineFormSet, LendingForm  # , MultiLendingForm,
from account.models import Player
from django.template import RequestContext
import datetime
from django_select2 import Select2View
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib import messages
from django.utils.translation import gettext_lazy as _


class ItemListView(ListView):
    model = Item
    template_name = 'lending/item_list.html'
    # filtrowanie po wypożyczonych, możliwość rezerwacji itd.


class LendGameView(DetailView):
    model = GameToLend
    template_name = 'lending/game_detail.html'


class LendItemView(DetailView):
    model = Item
    template_name = 'lending/item_detail.html'


#
# class ResolveLendView(CreateView):
# model = LendIt
#     template_name = 'base.html'
#     success_url = reverse_lazy('lending:item_list')
#
#     def post(self, request, *args, **kwargs):
#         now = datetime.datetime.now()
#         # FIXME: tu trzeba wykorzystać datę zapisując ją w formularzu - lub po prostu wywołać metodę return i z głowy.
#         super(ResolveLendView, self).post(request, *args, **kwargs)


def resolve_lend(request):
    # To jest BRZYDKIE i ZŁE
    user = request.user
    if not user.is_staff:  # TODO: zmienić na konkretne uprawnienia
        return HttpResponse('Unauthorized', status=401)

    return_url = request.GET.get('next', None)
    print 'GET: ', request.GET
    if request.method == 'POST':
        action = request.POST.get('action')
        if action == 'lend':  # wypożyczenie pojedynczej pozycji
            item_id = request.POST.get('item')
            item = get_object_or_404(Item, id=item_id)
            form = LendingForm(request.POST)
            if form.is_valid():
                form.save()
                days = form.cleaned_data['days']
                kwota = 0
                opcje = item.game.category.gamecategoryoption_set.order_by('-days').all()
                for opt in opcje:
                    if days >= opt.days:
                        x = days // opt.days
                        days -= x * opt.days
                        kwota += x * opt.get_price_for_item(item)
                messages.success(request, _('Należność: %(kwota)s zł' % {'kwota': kwota}))
                if bool(return_url):
                    return redirect(return_url)
                else:
                    return redirect('lending:item_detail', pk=item_id)
        elif action == 'return':  # zwrot pojedynczej pozycji
            item_id = request.POST.get('item')
            item = get_object_or_404(Item, id=item_id)
            dni, kwota = item.return_it()
            if dni == 0:
                messages.success(request, _('Gra nie była przetrzymana.'))
            else:
                messages.warning(request, _('Gra przetrzymana! Dni: %(dni)s, dopłata: %(kwota)s.') % {'dni': dni,
                                                                                                      'kwota': kwota})
        elif action == 'return_all':  # zwrot wszystkich pozycji danego użyszkodnika
            player = request.POST.get('player')
            items = LendIt.objects.filter(person__username=player, returned__isnull=True).distinct()
            kwota = 0
            for i in items:
                d, k = i.item.return_it()
                kwota += k
            if not kwota:
                messages.success(request, _('Gry pomyślnie zwrócone.'))
            else:
                messages.warning(request, _('Gry zwrócone. Należność za przetrzymanie: %.2f zł.' % kwota))
        elif action == 'add_to_cart':  # dodanie pozycji do koszyka
            cart, created = Cart.objects.get_or_create(owner=user)
            item = Item.objects.get(pk=request.POST.get('item'))
            if item.available:
                ci, created = CartItem.objects.get_or_create(cart=cart, item=item)
                ci.reserved = ci.reserved or user.is_staff
                ci.save()
                if user.is_staff and created:
                    messages.success(request, 'Pozycja została zarezerwowana (w koszyku).')
                elif created:
                    messages.success(request, 'Pozycja została dodana do koszyka.')
                else:
                    messages.warning(request, 'Pozycja znajduje się już w koszyku.')

            else:
                messages.error(request, "BŁĄD! Nie można zarezerwować.")
        elif action == 'remove_from_cart':  # usunięcie pozycji z koszyka
            cart, created = Cart.objects.get_or_create(owner=user)
            if created or not cart.items.all().count():
                messages.warning(request, 'Koszyk jest pusty; nie można niczego usunąć.')
            else:
                ci = CartItem.objects.get(cart=cart, item=request.POST.get('item'))
                ci.delete()
                messages.success(request, 'Pozycja została usunięta z koszyka.')
        elif action == 'multilend_or_extend':  # wypożyczenie lub przedłużenie kilku pozycji (zamknięcie koszyka)
            pass  # to będzie realizowane przez ShowCartView
    if bool(return_url):
        return redirect(return_url)
    return redirect('lending:item_list')


class ShowCart(TemplateView):
    template_name = 'lending/show_cart.html'

    def get_context_data(self, **kwargs):
        context = super(ShowCart, self).get_context_data(**kwargs)
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        cart, created = Cart.objects.get_or_create(owner=player)
        form = LendingForm()
        form.fields['person'].initial = player
        context.update({'player': player, 'cart': cart, 'form': form})
        return context

    # @method_decorator(permission_required('lending.add_lendit'))  # edit/change
    def dispatch(self, request, *args, **kwargs):
        return super(ShowCart, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')
        user = request.user
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        if action == 'save':
            to_del = [int(key.replace('item-del:', '')) for key, value in request.POST.iteritems() if
                      key.startswith('item-del:') if value]

            if user.is_staff:
                person_id = request.POST.get('person', None)
                person = Player.objects.get(id=person_id) if person_id else player
                to_lend = dict((int(key.replace('item-days:', '')), int(value or 0)) for key, value in request.POST.iteritems() if
                           key.startswith('item-days:') and int(key.replace('item-days:', '')) not in to_del)
                qs = Item.objects.filter(pk__in=to_lend).select_related()  # TODO: narrow select_related
                suma = 0
                for item_id, item_days in to_lend.iteritems():
                    item = qs.get(id=item_id)
                    lendit = LendIt.objects.create(item=item, person=person, days=item_days)
                    to_del.append(item_id)
                    opcje = item.game.category.gamecategoryoption_set.order_by('-days').all()
                    kwota = 0
                    days = item_days
                    for opt in opcje:
                        if days >= opt.days:
                            x = days // opt.days
                            days -= x * opt.days
                            kwota += x * opt.price
                    messages.info(request, 'Gra: %(name)s; dni: %(days)s; kwota: %(kwota)s' % {'name': item.game.name,
                                                                                               'days': item_days,
                                                                                               'kwota': kwota})
                    suma += kwota
                to_del_qs = Item.objects.filter(pk__in=to_del)
                CartItem.objects.filter(item__in=to_del_qs).delete()
                messages.success(request, 'Łączna kwota: %s' % suma)

            elif user.is_active:
                to_reserve = [key.replace('item-del:', '') for key, value in request.POST.iteritems() if
                              key.startswith('item-del:') if not value]
                qs = Item.objects.filter(pk__in=to_reserve).select_related()  # TODO: narrow select_related


        elif action == 'empty':

            if user.is_staff or user == player:
                Cart.objects.filter(owner=player).delete()
                messages.success(request, 'Koszyk został opróżniony.')
            else:
                messages.error(request, 'Błąd opróżniania koszyka.')

        return HttpResponseRedirect('/')  # może warto wracać do profilu użytkownika

    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)

# class MultiLendItCreateView(FormView):
#     template_name = 'multi_lendit.html'
#     success_url = reverse_lazy('lending:index')
#
#     def get(self, request, *args, **kwargs):
#         form = LendItInlineFormSet()
#         return self.render_to_response(self.get_context_data(form=form))
#
#     def post(self, request, *args, **kwargs):
#         form = LendItInlineFormSet()
#         if form.is_valid():
#             print u"formularz w porząsiu"
#             return self.form_valid(form)
#         return self.form_invalid(form)
#
#     # def form_valid(self, form):
#     #     form.save()
#

# def personal_lend(request, slug):
#     player = get_object_or_404(Player, username=slug)
#     PersonalLendFormSet = inlineformset_factory(Player, LendIt, fields=('item', 'option'), extra=1)
#     if request.method == 'POST':
#         formset = PersonalLendFormSet(request.POST, instance=player)
#         if formset.is_valid():
#             formset.save()
#     else:
#         formset = PersonalLendFormSet(instance=player)
#     return render_to_response('lending/personal_lend.html',
#                               {'formset': formset}
#     )
