app = angular.module "KlubStrefy", ["ui.bootstrap", "ngCookies", "ngResource"], ($interpolateProvider) ->
  $interpolateProvider.startSymbol("{$")
  $interpolateProvider.endSymbol("$}")

  return

app.factory 'APILendItem', ['$resource', ($resource) ->
  $resource '/api/v1/lend_item/:id/', id: '@id'
]


app.run ($http, $cookies) ->
  $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken']


app.controller 'ListCtrl',
  class ListCtrl
    constructor: ($scope, APILendItem) ->
      $scope.list_loading = true
      $scope.items = APILendItem.query()
      $scope.list_loading = false
