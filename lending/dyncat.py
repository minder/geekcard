# coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from .dyncat_base import RentCategoryTemplate, categories
from decimal import Decimal


class FlatRate(RentCategoryTemplate):
    name = _("Flat Rate")

    @classmethod
    def get_price(cls, item, option):
        return Decimal("%.2f" % option.value)

categories.register(FlatRate)


class DynamicRate(RentCategoryTemplate):
    name = _('Dynamic Rate')

    @classmethod
    def get_price(cls, item, option):
        return Decimal("%.2f" % (item.full_price * option.value / 100.))

categories.register(DynamicRate)
