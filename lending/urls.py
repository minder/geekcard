#coding: utf-8

from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import LendGameView, LendItemView  #, ResolveLendView

urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='lending/item_list.html'), name='index'),
    # url(r'^game/$', RedirectView.as_view(url=reverse_lazy('item:index'))),
    url(r'^game/(?P<pk>[\d]+)/$', LendGameView.as_view(), name='game_detail'),
    # url(r'^games/$', RedirectView.as_view(url=reverse_lazy('item:index'))),
    url(r'item/$', RedirectView.as_view(url=reverse_lazy('lending:index')), name='item_list'),
    url(r'items/$', RedirectView.as_view(url=reverse_lazy('lending:index'))),
    url(r'^item/(?P<pk>[\d]+)/$', LendItemView.as_view(), name='item_detail'),
    # url(r'^make_it_so/$', ResolveLendView.as_view(), name='make_it_so'),
    url(r'^make_it_so/$', 'lending.views.resolve_lend', name='make_it_so'),
)
