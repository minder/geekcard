# coding: utf-8
__author__ = 'minder'


class RentCategoryTemplate(object):
    name = "Template"

    @classmethod
    def get_price(cls, item, option):
        raise NotImplementedError


class RentCategoriesCache(object):
    def __init__(self):
        self._registry = {}

    def __getitem__(self, item):
        return self._registry[item]

    def get(self, item, default=None):
        try:
            return self._registry[item]
        except:
            return default

    def register(self, klass):
        """
        This method is used to register your statement to the global
        `clauses` list registry.

        :param klass: a class providing interface with affiliate network

        """
        name = klass.__module__ + '.' + klass.__name__
        self._registry[name] = klass
        return klass  # does it make this a decorator? :D

    def choices(self):
        """
        This method is used to populate choices field in the admin.

        :rtype: nested list in CHOICES format
        """
        result = []
        for key, klass in self._registry.items():
            result.append([key, klass.name])
        return result


categories = RentCategoriesCache()
