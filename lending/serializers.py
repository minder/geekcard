#coding: utf-8
from account.serializers import UserSerializer
from django.core.urlresolvers import reverse_lazy
from rest_framework import serializers
from .models import GameToLend, Item, LendIt


class GameToLendSerializer(serializers.ModelSerializer):
    link = serializers.SerializerMethodField('get_link')

    class Meta:
        model = GameToLend
        fields = ('id', 'name', 'full_price', 'shop_url', 'ean', 'pic', 'category', 'link')

    @staticmethod
    def get_link(obj):
        return reverse_lazy('lending:game_detail', kwargs={'pk': obj.pk})


class ItemSerializer(serializers.ModelSerializer):
    game = GameToLendSerializer()
    link = serializers.SerializerMethodField('get_link')
    available = serializers.BooleanField('available', read_only=True)
    reserved = serializers.BooleanField('reserved', read_only=True)
    overdue = serializers.IntegerField('overdue', read_only=True)
    when_will_be_back = serializers.DateField('when_will_be_back', read_only=True)
    lagger_link = serializers.SerializerMethodField('get_lagger_link')
    lagger_name = serializers.Field('lagger.get_full_name')

    class Meta:
        model = Item

    @staticmethod
    def get_link(obj):
        return reverse_lazy('lending:item_detail', kwargs={'pk': obj.pk})

    @staticmethod
    def get_lagger_link(obj):
        lagger = obj.lagger
        return reverse_lazy('player:detail', kwargs={'slug': lagger.username}) if lagger else None


class LendItSerializer(serializers.ModelSerializer):
    rental_date = serializers.DateTimeField(source='when')
    return_date = serializers.DateTimeField(source='returned')
    # person = UserSerializer()

    class Meta:
        model = LendIt
        fields = ('rental_date', 'return_date')
