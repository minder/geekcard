#coding: utf-8
from django.contrib import admin
from .models import GameCategory, GameCategoryOption, GameRating, GameToLend, LendIt, Item, CartItem, Cart
from django.utils.translation import ugettext_lazy as _


class MyAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    save_on_top = True


admin.site.register(
    [GameRating, Cart, CartItem], MyAdmin
)


class ItemAdmin(MyAdmin):
    list_display = ('game', 'barcode', )
    search_fields = ('game__name', 'barcode', )

admin.site.register(Item, ItemAdmin)


class GameCategoryOptionInline(admin.TabularInline):
    model = GameCategoryOption


class GameCategoryAdmin(MyAdmin):
    inlines = [
        GameCategoryOptionInline
    ]

admin.site.register(GameCategory, GameCategoryAdmin)


class GameToLendAdmin(MyAdmin):
    list_display = ('name', 'category', 'full_price', 'ean', 'get_ratings')
    list_filter = ('category',)
    search_fields = ('name', 'ean',)

    def get_ratings(obj):
        ranks = obj.get_ratings()
        return ranks

    get_ratings.short_description = _('rating')
    get_ratings = staticmethod(get_ratings)


admin.site.register(GameToLend, GameToLendAdmin)


class LendItAdmin(MyAdmin):
    list_display = ('item', 'person', 'when', 'returned')
    date_hierarchy = 'when'


admin.site.register(LendIt, LendItAdmin)


