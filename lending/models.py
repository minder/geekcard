#coding: utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils.timezone import now
from django_extensions.db.fields import ShortUUIDField
from account.models import Player
from django.db.models import Max, Avg, Min, Sum, Count
import datetime
from .dyncat import categories
import logging
# import select2.fields

from decimal import Decimal

logger = logging.getLogger('dev_debug')


class GameCategory(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(max_length=64)
    parser = models.CharField(max_length=200, choices=categories.choices(), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def __init__(self, *args, **kwargs):
        super(GameCategory, self).__init__(*args, **kwargs)
        self._handler = self.get_price_handler()

    def get_price_handler(self):
        return categories.get(self.parser, None)


class GameCategoryOption(models.Model):
    uuid = ShortUUIDField()
    days = models.PositiveSmallIntegerField(default=1)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=1)
    value = models.DecimalField(max_digits=5, decimal_places=2, default=1)
    category = models.ForeignKey(GameCategory)

    def __unicode__(self):
        return _("%(days)s dni  - %(price)s zł") % {'days': self.days, 'price': self.price}

    def get_price_for_item(self, item):
        try:
            return self.category._handler.get_price(item=item, option=self)
        except Exception as e:
            logger.error("Error %s: %s" % (type(e), e))
            return Decimal("%.2f" % self.price)


class GameToLend(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(max_length=256)
    desc = models.TextField(blank=True)
    ean = models.CharField(max_length=13, blank=True)
    category = models.ForeignKey(GameCategory)
    full_price = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    pic = models.ImageField(upload_to='lending/', blank=True)
    shop_url = models.URLField(help_text=_('Link do sklepu.'), blank=True)
    # related = models.ManyToManyField('GameToLend')  # trzeba by zadbać o to, by linkowanie było obustronne

    def __unicode__(self):
        return self.name

    def get_ratings(self):
        return self.ratings.filter(value__gte=0).aggregate(value=Avg('value'))['value']


class ReturnedItemsManager(models.Manager):
    def get_queryset(self):
        qs = super(ReturnedItemsManager, self).get_queryset()
        return qs.exclude(pk__in=Item.objects.filter(lendit__returned__isnull=True, lendit__isnull=False))


class Item(models.Model):
    """
    Konkretny egzemplarz gry
    """
    # game = models.ForeignKey(GameToLend)
    uuid = ShortUUIDField()
    game = models.ForeignKey(GameToLend)
    barcode = models.CharField(max_length=13, unique=True)
    borrowers = models.ManyToManyField(Player, through='LendIt', related_name='borrowed_items')
    # borrowers = select2.fields.ManyToManyField(Player, through='LendIt', ajax=True,
    #                                            related_name='borrowed_items', sort_field='when')
    objects = models.Manager()
    returned = ReturnedItemsManager()

    # przydałby się atrybut mówiący o tym, czy dany egzemplarz jest na stanie

    class Meta:
        ordering = ['game__name', 'barcode']

    @property
    def available(self):
        return self.lendit_set.filter(returned__isnull=True).count() == 0

    @property
    def reserved(self):
        return self.cartitem_set.filter(reserved=True).count() != 0

    def return_it(self):
        """
        Zwrot pożyczonej gry. Zwraca liczbę dni opóźnienia i cenę.
        """
        # TODO: poprawić obliczanie kwoty karnej - trzeba wyszukać domyślną cenę lub pobrać najmniejszą dla tej kategorii
        try:
            obj = LendIt.objects.get(item=self, returned__isnull=True)
            obj.returned = now()
            late = (obj.returned - obj.when).days - obj.days  # fix issue #4
            obj.late = late
            obj.save()
            if late > 0:
                price = GameCategoryOption.objects.filter(category=self.game.category).order_by('days', 'price')[0].price
                return late, late * price
            else:
                return 0, 0
        except LendIt.DoesNotExist:
            return 0, 0

    def __unicode__(self):
        return "%s %s" % (self.game.name, self.barcode)

    @property
    def when_will_be_back(self):
        lendit = self.lendit_set.filter(returned__isnull=True)
        if lendit.count():
            try:
                days = lendit[0].option.days
            except:
                days = lendit[0].days
            return lendit[0].when.date() + datetime.timedelta(days=days)
        return None

    @property
    def overdue(self):
        lendit = self.lendit_set.filter(returned__isnull=True)
        if lendit.count():
            try:
                days = lendit[0].option.days
            except:
                days = lendit[0].days
            od = (now() - lendit[0].when).days - days
            return od
        return None

    @property
    def lagger(self):
        lendit = self.lendit_set.filter(returned__isnull=True)
        if lendit:
            return lendit[0].person
        else:
            return None


class GameRating(models.Model):
    uuid = ShortUUIDField()
    person = models.ForeignKey(Player)
    game = models.ForeignKey(GameToLend, related_name='ratings')
    value = models.DecimalField(max_digits=2, decimal_places=1, default=-1)
    desc = models.TextField(blank=True)
    when = models.DateTimeField(auto_now=True)


class LendIt(models.Model):
    uuid = ShortUUIDField()
    item = models.ForeignKey(Item)
    person = models.ForeignKey(Player)
    option = models.ForeignKey(GameCategoryOption, help_text=_('Opcja wypożyczenia - na ile i za ile.'), null=True, blank=True, editable=False)  # DEPRECATED!
    days = models.PositiveSmallIntegerField(_('Ile dni'))
    # TODO: ograniczyć dostępność opcji cenowych w panelu admina
    when = models.DateTimeField(auto_now_add=True)
    returned = models.DateTimeField(null=True, blank=True,
                                    help_text=_('DATA ZWROTU. Data wypożyczenia dodawana jest automatycznie.'))
    late = models.SmallIntegerField(_('late'), default=0, editable=False)

    class Meta:
        ordering = ['-when']


class Cart(models.Model):
    uuid = ShortUUIDField()
    owner = models.ForeignKey(Player)
    items = models.ManyToManyField(Item, through='CartItem')

    def add_item(self, item):
        self.items.add(item)


class CartItem(models.Model):
    uuid = ShortUUIDField()
    item = models.ForeignKey(Item)
    cart = models.ForeignKey(Cart)
    added = models.DateTimeField(auto_now_add=True)
    reserved = models.BooleanField(default=False)


