#coding: utf-8

from django import template
from django.utils.timezone import now
from lending.models import LendIt, Item
from lending.forms import LendingForm, LendItInlineFormSet  #, MultiLendingForm, LendToPersonForm
register = template.Library()


@register.inclusion_tag('lending/tag_lending_form.html', takes_context=True)
def lending_form(context):
    item = context.get('object', context.get('item'))
    user = context.get('user')
    is_away = LendIt.objects.filter(item=item, returned__isnull=True)
    if is_away.count():
        lendit = is_away[0]
        lender = lendit.person
        is_away = bool(is_away.count())
    else:
        is_away = False
        lender = lendit = None
    form = LendingForm()
    return {
        'item': item,
        'is_away': is_away,
        'form': form,
        'lender': lender,
        'user': user,
    }


@register.inclusion_tag('lending/tag_lending_current.html', takes_context=True)
def lending_current(context):
    """
    Tag wyświetlający listę aktualnie wypożyczonych gier.
    Powinien umożliwiać zwrot po kliknięciu na przycisk.

    Zaznaczać przetrzymane gry wraz z informacją o dopłacie.
    """
    player = context.get('player')
    data = player.lendit_set.filter(returned__isnull=True).select_related().order_by('when').distinct()
    context.update({'items': data, 'now': now})
    return context


@register.inclusion_tag('lending/tag_lending_history.html', takes_context=True)
def lending_history(context, number=0):
    """
    Historia wypożyczeń dla danego gracza.
    Możliwość ograniczenia liczby wyświetlanych elementów (param. `number`)
    Bez podania tego parametru wyświetla wszystko.
    """
    player = context.get('player')
    data = player.lendit_set.filter(returned__isnull=False).order_by('-when', '-returned').distinct()
    if number > 0:
        data = data[:number]
    context.update({'items': data})
    return context


@register.filter
def remove_empty_carts(l):
    return [cart for cart in l if cart.items.count()]


@register.assignment_tag
def get_cart_items(player):
    return Item.objects.filter(cart__owner=player).distinct()


@register.inclusion_tag('lending/tag_rental_report.html')
def rental_report():
    return {'items': LendIt.objects.filter(returned__isnull=True).distinct().select_related()}  # TODO: optimize
