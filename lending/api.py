# coding: utf-8

import dateutil
from django.utils.datastructures import SortedDict
from rest_framework.response import Response
from rest_framework import viewsets
from .models import GameToLend, Item, LendIt
from .serializers import GameToLendSerializer, ItemSerializer, LendItSerializer
from rest_framework.decorators import action, link


class LendGameSet(viewsets.ModelViewSet):
    model = GameToLend
    serializer_class = GameToLendSerializer

    @action(methods=['get'])
    def lending_report(self, request, pk=None):
        kwargs = {'item__game__pk': pk}

        date_start = request.GET.get('date_start', None)
        if date_start is not None:
            kwargs['when__gte'] = dateutil.parser.parse(date_start)

        date_end = request.GET.get('date_end', None)
        if date_end is not None:
            kwargs['when__lte'] = dateutil.parser.parse(date_end)

        qs = LendIt.objects.filter(**kwargs).order_by('when').distinct()
        data = LendItSerializer(qs, many=True).data

        return Response(data)


class LendItemSet(viewsets.ModelViewSet):
    model = Item
    serializer_class = ItemSerializer
