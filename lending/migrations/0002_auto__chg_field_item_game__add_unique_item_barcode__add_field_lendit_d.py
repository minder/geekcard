# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Item.game'
        db.alter_column(u'lending_item', 'game_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameToLend']))
        # Adding unique constraint on 'Item', fields ['barcode']
        db.create_unique(u'lending_item', ['barcode'])

        # Adding field 'LendIt.days'
        db.add_column(u'lending_lendit', 'days',
                      self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0),
                      keep_default=False)


        # Changing field 'LendIt.option'
        db.alter_column(u'lending_lendit', 'option_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameCategoryOption'], null=True))

    def backwards(self, orm):
        # Removing unique constraint on 'Item', fields ['barcode']
        db.delete_unique(u'lending_item', ['barcode'])


        # Changing field 'Item.game'
        db.alter_column(u'lending_item', 'game_id', self.gf('select2.fields.ForeignKey')(to=orm['lending.GameToLend'], search_field=None))
        # Deleting field 'LendIt.days'
        db.delete_column(u'lending_lendit', 'days')


        # User chose to not deal with backwards NULL issues for 'LendIt.option'
        raise RuntimeError("Cannot reverse this migration. 'LendIt.option' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration
        # Changing field 'LendIt.option'
        db.alter_column(u'lending_lendit', 'option_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameCategoryOption']))

    models = {
        u'account.player': {
            'Meta': {'ordering': "[u'last_name']", 'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'lending.gamecategory': {
            'Meta': {'object_name': 'GameCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'lending.gamecategoryoption': {
            'Meta': {'object_name': 'GameCategoryOption'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameCategory']"}),
            'days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '1', 'max_digits': '5', 'decimal_places': '2'})
        },
        u'lending.gamerating': {
            'Meta': {'object_name': 'GameRating'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'ratings'", 'to': u"orm['lending.GameToLend']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': '-1', 'max_digits': '2', 'decimal_places': '1'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'lending.gametolend': {
            'Meta': {'object_name': 'GameToLend'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameCategory']"}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ean': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'full_price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '7', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'shop_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'lending.item': {
            'Meta': {'ordering': "[u'game__name', u'barcode']", 'object_name': 'Item'},
            'barcode': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '13'}),
            'borrowers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'borrowed_items'", 'symmetrical': 'False', 'through': u"orm['lending.LendIt']", 'to': u"orm['account.Player']"}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameToLend']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'lending.lendit': {
            'Meta': {'object_name': 'LendIt'},
            'days': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.Item']"}),
            'option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameCategoryOption']", 'null': 'True', 'blank': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'returned': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['lending']