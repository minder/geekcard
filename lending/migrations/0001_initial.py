# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GameCategory'
        db.create_table(u'lending_gamecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'lending', ['GameCategory'])

        # Adding model 'GameCategoryOption'
        db.create_table(u'lending_gamecategoryoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('days', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=1, max_digits=5, decimal_places=2)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameCategory'])),
        ))
        db.send_create_signal(u'lending', ['GameCategoryOption'])

        # Adding model 'GameToLend'
        db.create_table(u'lending_gametolend', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('ean', self.gf('django.db.models.fields.CharField')(max_length=13, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameCategory'])),
            ('full_price', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=7, decimal_places=2)),
            ('pic', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('shop_url', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'lending', ['GameToLend'])

        # Adding model 'Item'
        db.create_table(u'lending_item', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameToLend'])),
            ('barcode', self.gf('django.db.models.fields.CharField')(max_length=13)),
        ))
        db.send_create_signal(u'lending', ['Item'])

        # Adding model 'GameRating'
        db.create_table(u'lending_gamerating', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('person', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Player'])),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'ratings', to=orm['lending.GameToLend'])),
            ('value', self.gf('django.db.models.fields.DecimalField')(default=-1, max_digits=2, decimal_places=1)),
            ('desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('when', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'lending', ['GameRating'])

        # Adding model 'LendIt'
        db.create_table(u'lending_lendit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.Item'])),
            ('person', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Player'])),
            ('option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lending.GameCategoryOption'])),
            ('when', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('returned', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'lending', ['LendIt'])


    def backwards(self, orm):
        # Deleting model 'GameCategory'
        db.delete_table(u'lending_gamecategory')

        # Deleting model 'GameCategoryOption'
        db.delete_table(u'lending_gamecategoryoption')

        # Deleting model 'GameToLend'
        db.delete_table(u'lending_gametolend')

        # Deleting model 'Item'
        db.delete_table(u'lending_item')

        # Deleting model 'GameRating'
        db.delete_table(u'lending_gamerating')

        # Deleting model 'LendIt'
        db.delete_table(u'lending_lendit')


    models = {
        u'account.player': {
            'Meta': {'ordering': "[u'last_name']", 'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'lending.gamecategory': {
            'Meta': {'object_name': 'GameCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'lending.gamecategoryoption': {
            'Meta': {'object_name': 'GameCategoryOption'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameCategory']"}),
            'days': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '1', 'max_digits': '5', 'decimal_places': '2'})
        },
        u'lending.gamerating': {
            'Meta': {'object_name': 'GameRating'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'ratings'", 'to': u"orm['lending.GameToLend']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'value': ('django.db.models.fields.DecimalField', [], {'default': '-1', 'max_digits': '2', 'decimal_places': '1'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'lending.gametolend': {
            'Meta': {'object_name': 'GameToLend'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameCategory']"}),
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ean': ('django.db.models.fields.CharField', [], {'max_length': '13', 'blank': 'True'}),
            'full_price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '7', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'pic': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'shop_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'lending.item': {
            'Meta': {'object_name': 'Item'},
            'barcode': ('django.db.models.fields.CharField', [], {'max_length': '13'}),
            'borrowers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'borrowed_items'", 'symmetrical': 'False', 'through': u"orm['lending.LendIt']", 'to': u"orm['account.Player']"}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameToLend']"}),
            'game': ('select2.fields.ForeignKey', [], {'to': u"orm['lending.GameToLend']", 'search_field': 'None'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'lending.lendit': {
            'Meta': {'object_name': 'LendIt'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.Item']"}),
            'option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['lending.GameCategoryOption']"}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'returned': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['lending']
