# coding: utf-8
__author__ = 'minder'

from django.forms import ModelForm, Form
from django.forms.models import inlineformset_factory

from .models import Item, LendIt, Player, GameCategoryOption
# from select2.fields import MultipleChoiceField, ForeignKey, ModelChoiceField, ManyToManyField

from django_select2 import AutoModelSelect2MultipleField, ModelSelect2Field, AutoModelSelect2Field
from .utils import as_choices


# ################# FIELDS #################
#
# class MultiItemsField(AutoModelSelect2MultipleField):
#     queryset = Item.returned
#     search_fields = ['game__name__icontains', 'barcode__contains']
#
#
class PlayerS2Field(AutoModelSelect2Field):
    queryset = Player.objects
    search_fields = ['first_name__icontains', 'last_name__icontains', 'club_cards__barcode__contains']

#
# ################# FORMS #################
#

class LendingForm(ModelForm):
    # item = select2.fields.ForeignKey(Item)
    person = PlayerS2Field()

    class Meta:
        model = LendIt

    # def __init__(self, *args, **kwargs):
    #     if 'item' in kwargs:
    #         item = kwargs.pop('item')
    #     else:
    #         item = None
    #     super(LendingForm, self).__init__(*args, **kwargs)
    #     if item is not None:
    #         self.fields['option'].queryset = GameCategoryOption.objects.filter(category=item.game.category)


class ReturnForm(ModelForm):
    class Meta:
        model = LendIt

#
# class MultiLendingForm(Form):
#     # items = MultipleChoiceField(choices=Item.returned.as_choices())
#     # items = django_select2.fields.HeavySelect2MultipleChoiceField(choices=Item.returned.as_choices())
#     items = MultiItemsField()
#
#
# class LendToPersonForm(ModelForm):
#     class Meta:
#         model = Player
#         fields = ['id']


LendItInlineFormSet = inlineformset_factory(Player, LendIt, fields=['item', 'days'], extra=1)

