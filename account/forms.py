#coding: utf-8
from localflavor.pl.forms import PLPESELField

__author__ = 'minder'

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext, ugettext_lazy as _

user_model = get_user_model()


class MyUserCreationForm(UserCreationForm):
    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            user_model._default_manager.get(username=username)
        except user_model.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    class Meta(UserCreationForm.Meta):
        model = user_model


class QuickPlayerRegistrationForm(forms.Form):
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'duplicate_email': _("A user with that email already exists."),
    }
    first_name = forms.CharField(max_length=30, label=u'Imię')
    last_name = forms.CharField(max_length=30, label='Nazwisko')
    email = forms.EmailField()
    username = forms.RegexField(label='Login', max_length=30, regex=r'^[\w.-]+$',
                                help_text=_(u"Max 30 znaków. Tylko litery, cyfry i znaki . - _"),
                                error_messages={
                                    'invalid': _(u"Pole może zawierać wyłącznie litery, cyfry oraz znaki "
                                                 u". - _ ")})
    card_number = forms.IntegerField(required=False, label='Nr karty klubowej')
    phone = forms.CharField(max_length=12, label='Telefon', required=False)
    address = forms.CharField(label='Adres', widget=forms.Textarea, required=False)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            user_model._default_manager.get(username=username)
        except user_model.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            user_model._default_manager.get(email=email)
        except user_model.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])
