#coding: utf-8
__author__ = 'minder'

from .models import ClubCard, Player
from .serializers import UserSerializer, ClubCardSerializer
from rest_framework import viewsets


class UserViewSet(viewsets.ModelViewSet):
    model = Player
    serializer_class = UserSerializer


class ClubCardViewSet(viewsets.ModelViewSet):
    model = ClubCard
    serializer_class = ClubCardSerializer
