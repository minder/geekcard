#coding: utf-8
__author__ = 'minder'
from django.contrib import admin
from .models import ClubCard, Player, PlayerProfile
from league.admin import GameIDInline
from django.contrib.auth.admin import UserAdmin
from .forms import MyUserCreationForm


class MyAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    save_on_top = True


class ProfileInline(admin.StackedInline):
    model = PlayerProfile


class PlayerAdmin(UserAdmin):
    date_hierarchy = 'date_joined'
    inlines = [ProfileInline, GameIDInline, ]
    save_on_top = True
    add_form = MyUserCreationForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')}
        ),
    )
    prepopulated_fields = {'username': ('first_name', 'last_name',)}
    ordering = ['last_name']
    search_fields = ['last_name', 'club_cards__barcode', 'gameid__number']


admin.site.register(Player, PlayerAdmin)


class ClubCardAdmin(MyAdmin):
    list_display = list_display_links = ['owner', 'barcode', 'date_issued', 'active']
    date_hierarchy = 'date_issued'
    search_fields = ['owner__last_name', 'barcode', 'owner__gameid__number']
    list_filter = ['active']
    ordering = ['barcode']


admin.site.register(ClubCard, ClubCardAdmin)

