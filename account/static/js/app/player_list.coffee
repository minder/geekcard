app = angular.module "KlubStrefy", ["ui.bootstrap", "ngCookies", "ngResource"], ($interpolateProvider) ->
  $interpolateProvider.startSymbol("{$")
  $interpolateProvider.endSymbol("$}")

  return

app.factory 'APIUser', ['$resource', ($resource) ->
  $resource '/api/v1/user/:id/', id: '@id'
]


app.run ($http, $cookies) ->
  $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken']


app.controller 'ListCtrl',
  class ListCtrl
    constructor: ($scope, APIUser) ->
      $scope.list_load = true
      $scope.users = APIUser.query()
      $scope.list_load = false
