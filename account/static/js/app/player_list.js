// Generated by CoffeeScript 1.3.3
(function() {
  var ListCtrl, app;

  app = angular.module("KlubStrefy", ["ui.bootstrap", "ngCookies", "ngResource"], function($interpolateProvider) {
    $interpolateProvider.startSymbol("{$");
    $interpolateProvider.endSymbol("$}");
  });

  app.factory('APIUser', [
    '$resource', function($resource) {
      return $resource('/api/v1/user/:id/', {
        id: '@id'
      });
    }
  ]);

  app.run(function($http, $cookies) {
    return $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
  });

  app.controller('ListCtrl', ListCtrl = (function() {

    function ListCtrl($scope, APIUser) {
      $scope.list_load = true;
      $scope.users = APIUser.query();
      $scope.list_load = false;
    }

    return ListCtrl;

  })());

}).call(this);
