#coding: utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager, SiteProfileNotAvailable
from django.core.urlresolvers import reverse_lazy
import re
import warnings

from django.core.exceptions import ImproperlyConfigured
from django.core.mail import send_mail
from django.core import validators
from django.db import models
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django_extensions.db.fields import ShortUUIDField


class Player(AbstractBaseUser, PermissionsMixin):
    uuid = ShortUUIDField()
    username = models.CharField(_('username'), max_length=100, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, numbers and '
                                            '@/./+/-/_ characters'),
                                validators=[
                                    validators.RegexValidator(re.compile('^[\w.@+-]+$'), _('Enter a valid username.'),
                                                              'invalid')
                                ])
    first_name = models.CharField(_('first name'), max_length=30, blank=True, db_index=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, db_index=True)
    email = models.EmailField(_('email address'), blank=True, db_index=True, unique=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('player')
        verbose_name_plural = _('players')
        ordering = ['last_name']

    def get_absolute_url(self):
        return reverse_lazy('player:detail', kwargs={'slug': self.username})

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = u'%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def get_profile(self):
        """
        Returns site-specific profile for this user. Raises
        SiteProfileNotAvailable if this site does not allow profiles.
        """
        warnings.warn("The use of AUTH_PROFILE_MODULE to define user profiles has been deprecated.",
                      DeprecationWarning, stacklevel=2)
        if not hasattr(self, '_profile_cache'):
            from django.conf import settings

            if not getattr(settings, 'AUTH_PROFILE_MODULE', False):
                raise SiteProfileNotAvailable(
                    'You need to set AUTH_PROFILE_MODULE in your project '
                    'settings')
            try:
                app_label, model_name = settings.AUTH_PROFILE_MODULE.split('.')
            except ValueError:
                raise SiteProfileNotAvailable(
                    'app_label and model_name should be separated by a dot in '
                    'the AUTH_PROFILE_MODULE setting')
            try:
                model = models.get_model(app_label, model_name)
                if model is None:
                    raise SiteProfileNotAvailable(
                        'Unable to load the profile model, check '
                        'AUTH_PROFILE_MODULE in your project settings')
                self._profile_cache = model._default_manager.using(
                    self._state.db).get(user__id__exact=self.id)
                self._profile_cache.user = self
            except (ImportError, ImproperlyConfigured):
                raise SiteProfileNotAvailable
        return self._profile_cache

    def get_repr(self):
        cards = self.club_cards.filter(active=True).order_by('-date_issued')
        if cards.count() == 0:
            card_number = '[brak karty klubowej]'
        else:
            card_number = cards[0].barcode
        try:
            return u"%s | %s" % (self.get_full_name() or self.get_username(), card_number)
        except:
            return u"%s | %s" % (self.get_username(), card_number)

    def __str__(self):
        return self.get_repr()

    def __unicode__(self):
        return self.get_repr()


class PlayerProfile(models.Model):
    uuid = ShortUUIDField()
    player = models.OneToOneField(Player, related_name='profile')
    phone = models.CharField(max_length=20, blank=True)
    address = models.TextField(blank=True)

    def __unicode__(self):
        return self.player


class ClubCard(models.Model):
    uuid = ShortUUIDField()
    owner = models.ForeignKey(Player, verbose_name=_('owner'), related_name='club_cards')
    barcode = models.CharField(_('barcode'), max_length=64, db_index=True)
    active = models.BooleanField(_('active'), default=True)
    date_issued = models.DateTimeField(_('date issued'), auto_now_add=True)

    def __unicode__(self):
        return "%s - %s" % (self.barcode, self.owner)

    class Meta:
        ordering = ['-date_issued']

    def save(self, *args, **kwargs):
        if self.active:
            stare = ClubCard.objects.filter(owner=self.owner)
            stare.update(active=False)
        super(ClubCard, self).save(*args, **kwargs)
