#coding: utf-8
from django.conf import settings
from django.contrib.auth.decorators import permission_required
from django.contrib.sites.models import Site, RequestSite
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import View, FormView
from registration import signals
from registration.backends.default.views import RegistrationView
from registration.models import RegistrationProfile
from account.forms import QuickPlayerRegistrationForm
from account.models import ClubCard, PlayerProfile
from account.utils import pwgen

try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User


class RegisterPlayerView(RegistrationView):
    form_class = QuickPlayerRegistrationForm

    @method_decorator(permission_required('account.add_player'))
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterPlayerView, self).dispatch(request, *args, **kwargs)

    def registration_allowed(self, request):
        return True

    def register(self, request, **cleaned_data):
        first_name, last_name = cleaned_data['first_name'], cleaned_data['last_name']
        username,  email = cleaned_data['username'], cleaned_data['email']
        address = cleaned_data['address']
        phone = cleaned_data['phone']
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        password = pwgen()
        new_user = User.objects.create_user(username, email, password)
        new_user.is_active = True
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.save()
        profile = PlayerProfile.objects.create(player=new_user, phone=phone, address=address)

        card_number = cleaned_data['card_number']
        if card_number:
            ClubCard.objects.create(barcode=card_number, owner=new_user, active=True)

        ctx_dict = {'password': password,
                    'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                    'user': new_user,
                    'site': site,
                    }
        subject = render_to_string('registration/activation_email_subject.txt', ctx_dict)
        message = render_to_string('registration/activation_email.txt', ctx_dict)

        new_user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)
        signals.user_registered.send(sender=self.__class__, user=new_user, request=request)

        return new_user


class EditPlayerDetailsView(FormView):
    pass
