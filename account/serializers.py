#coding: utf-8
__author__ = 'minder'
from django.core.urlresolvers import reverse_lazy
from rest_framework import serializers
from .models import Player, ClubCard


class ClubCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClubCard


class UserSerializer(serializers.ModelSerializer):
    club_cards = ClubCardSerializer()
    club_card = serializers.SerializerMethodField('get_clubcard')
    link = serializers.SerializerMethodField('get_link')

    @staticmethod
    def get_link(obj):
        return reverse_lazy('player:detail', kwargs={'slug': obj.username})

    @staticmethod
    def get_clubcard(obj):
        cards = ClubCard.objects.filter(active=True, owner=obj)
        if cards:
            card = cards[0]
            serializer = ClubCardSerializer(card)
            return serializer.data
        else:
            return ''

    class Meta:
        model = Player
        fields = ('id', 'username', 'club_card', 'club_cards', 'first_name', 'last_name', 'link')
