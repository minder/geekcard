#coding: utf-8
from __future__ import unicode_literals
from django.conf import settings

FONTAWESOME_DEFAULTS = {
    'url': '//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css',
}

FONTAWESOME = FONTAWESOME_DEFAULTS.copy()

FONTAWESOME.update(getattr(settings, 'FONTAWESOME', {}))


def fontawesome_css_url():
    return FONTAWESOME['url']
