#coding: utf-8

from django import template
from ..defaults import fontawesome_css_url

register = template.Library()


@register.simple_tag
def fa_css():
    url = fontawesome_css_url()
    return '<link href="{url}" rel="stylesheet">'.format(url=url)


@register.simple_tag
def fa_icon(icon, alt_text='Awesome Icon'):
    """
    Render an icon.
    Cheat sheet: http://fortawesome.github.io/Font-Awesome/cheatsheet/
    """
    return u'<i class="fa fa-{icon}" alt="{alt}"></i>'.format(icon=icon, alt=alt_text)

