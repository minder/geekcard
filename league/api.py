#coding: utf-8
__author__ = 'minder'

from rest_framework import viewsets
from .models import Game, GameID, LeagueSeason, EventCategory, Match, MatchResult, Reward


class GameViewSet(viewsets.ModelViewSet):
    model = Game


class GameIDViewSet(viewsets.ModelViewSet):
    model = GameID


class LeagueSeasonViewSet(viewsets.ModelViewSet):
    model = LeagueSeason


class EventCategoryViewSet(viewsets.ModelViewSet):
    model = EventCategory


class MatchViewSet(viewsets.ModelViewSet):
    model = Match


class MatchResultViewSet(viewsets.ModelViewSet):
    model = MatchResult


class RewardViewSet(viewsets.ModelViewSet):
    model = Reward

