# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Game'
        db.create_table(u'league_game', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
        ))
        db.send_create_signal(u'league', ['Game'])

        # Adding model 'GameIDName'
        db.create_table(u'league_gameidname', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'league', ['GameIDName'])

        # Adding model 'GameID'
        db.create_table(u'league_gameid', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.Game'])),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Player'])),
            ('number', self.gf('django.db.models.fields.CharField')(max_length=64, db_index=True)),
            ('name', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.GameIDName'])),
        ))
        db.send_create_signal(u'league', ['GameID'])

        # Adding model 'RewardCategory'
        db.create_table(u'league_rewardcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64, db_index=True)),
            ('value', self.gf('django.db.models.fields.SmallIntegerField')()),
        ))
        db.send_create_signal(u'league', ['RewardCategory'])

        # Adding model 'LeagueSeason'
        db.create_table(u'league_leagueseason', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.Game'])),
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('points_for_winning', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=3)),
            ('points_for_losing', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('points_for_draw', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'league', ['LeagueSeason'])

        # Adding M2M table for field players on 'LeagueSeason'
        m2m_table_name = db.shorten_name(u'league_leagueseason_players')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('leagueseason', models.ForeignKey(orm[u'league.leagueseason'], null=False)),
            ('player', models.ForeignKey(orm[u'account.player'], null=False))
        ))
        db.create_unique(m2m_table_name, ['leagueseason_id', 'player_id'])

        # Adding M2M table for field rewards on 'LeagueSeason'
        m2m_table_name = db.shorten_name(u'league_leagueseason_rewards')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('leagueseason', models.ForeignKey(orm[u'league.leagueseason'], null=False)),
            ('rewardcategory', models.ForeignKey(orm[u'league.rewardcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['leagueseason_id', 'rewardcategory_id'])

        # Adding model 'EventCategory'
        db.create_table(u'league_eventcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('reward_multiplier', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('max_players', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'league', ['EventCategory'])

        # Adding model 'Tournament'
        db.create_table(u'league_tournament', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.EventCategory'])),
        ))
        db.send_create_signal(u'league', ['Tournament'])

        # Adding M2M table for field players on 'Tournament'
        m2m_table_name = db.shorten_name(u'league_tournament_players')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('tournament', models.ForeignKey(orm[u'league.tournament'], null=False)),
            ('player', models.ForeignKey(orm[u'account.player'], null=False))
        ))
        db.create_unique(m2m_table_name, ['tournament_id', 'player_id'])

        # Adding model 'Match'
        db.create_table(u'league_match', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.EventCategory'])),
            ('season', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.LeagueSeason'])),
            ('when', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'league', ['Match'])

        # Adding model 'MatchResult'
        db.create_table(u'league_matchresult', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Player'])),
            ('match', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.Match'])),
            ('games_won', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('points', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'league', ['MatchResult'])

        # Adding model 'Reward'
        db.create_table(u'league_reward', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Player'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['league.RewardCategory'])),
            ('when', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'league', ['Reward'])


    def backwards(self, orm):
        # Deleting model 'Game'
        db.delete_table(u'league_game')

        # Deleting model 'GameIDName'
        db.delete_table(u'league_gameidname')

        # Deleting model 'GameID'
        db.delete_table(u'league_gameid')

        # Deleting model 'RewardCategory'
        db.delete_table(u'league_rewardcategory')

        # Deleting model 'LeagueSeason'
        db.delete_table(u'league_leagueseason')

        # Removing M2M table for field players on 'LeagueSeason'
        db.delete_table(db.shorten_name(u'league_leagueseason_players'))

        # Removing M2M table for field rewards on 'LeagueSeason'
        db.delete_table(db.shorten_name(u'league_leagueseason_rewards'))

        # Deleting model 'EventCategory'
        db.delete_table(u'league_eventcategory')

        # Deleting model 'Tournament'
        db.delete_table(u'league_tournament')

        # Removing M2M table for field players on 'Tournament'
        db.delete_table(db.shorten_name(u'league_tournament_players'))

        # Deleting model 'Match'
        db.delete_table(u'league_match')

        # Deleting model 'MatchResult'
        db.delete_table(u'league_matchresult')

        # Deleting model 'Reward'
        db.delete_table(u'league_reward')


    models = {
        u'account.player': {
            'Meta': {'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'league.eventcategory': {
            'Meta': {'object_name': 'EventCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_players': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'reward_multiplier': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'})
        },
        u'league.game': {
            'Meta': {'object_name': 'Game'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'league.gameid': {
            'Meta': {'object_name': 'GameID'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.GameIDName']"}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"})
        },
        u'league.gameidname': {
            'Meta': {'object_name': 'GameIDName'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'league.leagueseason': {
            'Meta': {'object_name': 'LeagueSeason'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'symmetrical': 'False'}),
            'points_for_draw': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'points_for_losing': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'points_for_winning': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'rewards': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['league.RewardCategory']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'start_date': ('django.db.models.fields.DateField', [], {})
        },
        u'league.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'through': u"orm['league.MatchResult']", 'symmetrical': 'False'}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.LeagueSeason']"}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'league.matchresult': {
            'Meta': {'object_name': 'MatchResult'},
            'games_won': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Match']"}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'points': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'})
        },
        u'league.reward': {
            'Meta': {'object_name': 'Reward'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.RewardCategory']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'league.rewardcategory': {
            'Meta': {'object_name': 'RewardCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        u'league.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'symmetrical': 'False'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['league']