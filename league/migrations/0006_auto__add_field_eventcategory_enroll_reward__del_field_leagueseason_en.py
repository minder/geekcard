# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'EventCategory.enroll_reward'
        db.add_column(u'league_eventcategory', 'enroll_reward',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='conf_as_enroll', to=orm['league.RewardCategory']),
                      keep_default=False)

        # Deleting field 'LeagueSeason.enroll_reward'
        db.delete_column(u'league_leagueseason', 'enroll_reward_id')


    def backwards(self, orm):
        # Deleting field 'EventCategory.enroll_reward'
        db.delete_column(u'league_eventcategory', 'enroll_reward_id')

        # Adding field 'LeagueSeason.enroll_reward'
        db.add_column(u'league_leagueseason', 'enroll_reward',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='conf_as_enroll', to=orm['league.RewardCategory']),
                      keep_default=False)


    models = {
        u'account.player': {
            'Meta': {'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'league.eventcategory': {
            'Meta': {'object_name': 'EventCategory'},
            'enroll_reward': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'conf_as_enroll'", 'to': u"orm['league.RewardCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_players': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'reward_multiplier': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'})
        },
        u'league.game': {
            'Meta': {'object_name': 'Game'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'points_for_draw': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'points_for_losing': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'points_for_winning': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'reporter_tool': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'rewards': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['league.RewardCategory']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'league.gameid': {
            'Meta': {'object_name': 'GameID'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"})
        },
        u'league.leagueseason': {
            'Meta': {'object_name': 'LeagueSeason'},
            'draw_reward': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'conf_as_draw'", 'to': u"orm['league.RewardCategory']"}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lose_reward': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'conf_as_lose'", 'to': u"orm['league.RewardCategory']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'win_reward': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'conf_as_win'", 'to': u"orm['league.RewardCategory']"})
        },
        u'league.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'through': u"orm['league.MatchResult']", 'symmetrical': 'False'}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.LeagueSeason']"}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Tournament']", 'null': 'True', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'league.matchresult': {
            'Meta': {'object_name': 'MatchResult'},
            'games_won': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Match']"}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'points': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'reward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Reward']", 'null': 'True', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'league.reward': {
            'Meta': {'object_name': 'Reward'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.RewardCategory']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {}),
            'when': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'league.rewardcategory': {
            'Meta': {'object_name': 'RewardCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        u'league.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 1, 9, 0, 0)'}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'reporter_tool': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'result': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        }
    }

    complete_apps = ['league']