# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'EventCategory.uuid'
        db.add_column(u'league_eventcategory', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'Game.uuid'
        db.add_column(u'league_game', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'MatchResult.uuid'
        db.add_column(u'league_matchresult', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'PromoItem.uuid'
        db.add_column(u'league_promoitem', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'RewardCategory.uuid'
        db.add_column(u'league_rewardcategory', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'Reward.uuid'
        db.add_column(u'league_reward', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'Match.uuid'
        db.add_column(u'league_match', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'LeagueSeason.uuid'
        db.add_column(u'league_leagueseason', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'Tournament.uuid'
        db.add_column(u'league_tournament', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'LeagueEnroll.uuid'
        db.add_column(u'league_leagueenroll', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)

        # Adding field 'GameID.uuid'
        db.add_column(u'league_gameid', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=22, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'EventCategory.uuid'
        db.delete_column(u'league_eventcategory', 'uuid')

        # Deleting field 'Game.uuid'
        db.delete_column(u'league_game', 'uuid')

        # Deleting field 'MatchResult.uuid'
        db.delete_column(u'league_matchresult', 'uuid')

        # Deleting field 'PromoItem.uuid'
        db.delete_column(u'league_promoitem', 'uuid')

        # Deleting field 'RewardCategory.uuid'
        db.delete_column(u'league_rewardcategory', 'uuid')

        # Deleting field 'Reward.uuid'
        db.delete_column(u'league_reward', 'uuid')

        # Deleting field 'Match.uuid'
        db.delete_column(u'league_match', 'uuid')

        # Deleting field 'LeagueSeason.uuid'
        db.delete_column(u'league_leagueseason', 'uuid')

        # Deleting field 'Tournament.uuid'
        db.delete_column(u'league_tournament', 'uuid')

        # Deleting field 'LeagueEnroll.uuid'
        db.delete_column(u'league_leagueenroll', 'uuid')

        # Deleting field 'GameID.uuid'
        db.delete_column(u'league_gameid', 'uuid')


    models = {
        u'account.player': {
            'Meta': {'ordering': "[u'last_name']", 'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'league.eventcategory': {
            'Meta': {'object_name': 'EventCategory'},
            'enroll_reward': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'conf_as_enroll'", 'null': 'True', 'to': u"orm['league.RewardCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_players': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'reward_multiplier': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        },
        u'league.game': {
            'Meta': {'object_name': 'Game'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'points_for_draw': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'points_for_losing': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'points_for_winning': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '3'}),
            'reporter_tool': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'rewards': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['league.RewardCategory']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        },
        u'league.gameid': {
            'Meta': {'unique_together': "((u'game', u'number'),)", 'object_name': 'GameID'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        },
        u'league.leagueenroll': {
            'Meta': {'unique_together': "((u'player', u'season'),)", 'object_name': 'LeagueEnroll'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'reward_given': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.LeagueSeason']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        },
        u'league.leagueseason': {
            'Meta': {'ordering': "[u'slug']", 'object_name': 'LeagueSeason'},
            'badge_color': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'default_match_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']", 'null': 'True', 'blank': 'True'}),
            'draw_reward': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'conf_as_draw'", 'null': 'True', 'to': u"orm['league.RewardCategory']"}),
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Game']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lose_reward': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'conf_as_lose'", 'null': 'True', 'to': u"orm['league.RewardCategory']"}),
            'max_matches': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'through': u"orm['league.LeagueEnroll']", 'symmetrical': 'False'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'}),
            'win_reward': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'conf_as_win'", 'null': 'True', 'to': u"orm['league.RewardCategory']"})
        },
        u'league.match': {
            'Meta': {'object_name': 'Match'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ignore': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'players': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['account.Player']", 'through': u"orm['league.MatchResult']", 'symmetrical': 'False'}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.LeagueSeason']"}),
            'tournament': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Tournament']", 'null': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'league.matchresult': {
            'Meta': {'object_name': 'MatchResult'},
            'games_won': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'match': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Match']"}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'points': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'reward': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.Reward']", 'null': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'league.promoitem': {
            'Meta': {'object_name': 'PromoItem'},
            'desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'reward_category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.RewardCategory']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        },
        u'league.reward': {
            'Meta': {'object_name': 'Reward'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.RewardCategory']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orig_value': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.LeagueSeason']"}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'when': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'})
        },
        u'league.rewardcategory': {
            'Meta': {'object_name': 'RewardCategory'},
            'eternal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64', 'db_index': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'}),
            'value': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        u'league.tournament': {
            'Meta': {'object_name': 'Tournament'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.EventCategory']"}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'parsed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reporter_tool': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'result': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'season': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['league.LeagueSeason']"}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '22', 'blank': 'True'})
        }
    }

    complete_apps = ['league']