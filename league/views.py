#coding: utf-8
from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, FormView, View, RedirectView
from django.shortcuts import get_object_or_404, redirect
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from .models import Reward, LeagueSeason, Match, Player, LeagueEnroll
from .forms import PlayerMatchForm, RewardForm, SeasonEnrollForm


def nullify_old_rewards(request):
    current_seasons = LeagueSeason.objects.filter(start_date__lte=now, end_date__gte=now)
    oldies = Reward.objects.exclude(category__eternal=True)
    for season in current_seasons:
        # zabezpiecz nagrody przyznane później niż 'długość obecnego sezonu' przed początkiem obecnego sezonu
        oldies = oldies.exclude(when__gte=season.start_date - (season.end_date - season.start_date))
    oldies.update(value=0, comment='[Automat] Przedawnione')


class PlayerMatchHistoryView(ListView):
    model = Match
    template_name = 'league/player_match_history.html'

    def get_queryset(self):
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        result = Match.objects.filter(matchresult__player=player).order_by('-when')
        return result

    def get_context_data(self, **kwargs):
        context = super(PlayerMatchHistoryView, self).get_context_data(**kwargs)
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        context.update({'player': player})
        return context


class PlayerStampHistoryView(ListView):
    model = Reward
    template_name = 'league/player_stamp_history.html'

    def get_queryset(self):
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        result = Reward.objects.filter(player=player).order_by('-when')
        return result

    def get_context_data(self, **kwargs):
        context = super(PlayerStampHistoryView, self).get_context_data(**kwargs)
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        context.update({'player': player})
        return context

    def dispatch(self, request, *args, **kwargs):
        player = get_object_or_404(Player, username=self.kwargs['slug'])
        if request.user == player or request.user.is_staff:
            return super(PlayerStampHistoryView, self).dispatch(request, *args, **kwargs)
        else:
            raise PermissionDenied


def my_profile_view(request):
    me = request.user
    return redirect('player:detail', slug=me.username)


class PlayerStampAddView(CreateView):
    model = Reward
    form_class = RewardForm
    fields = ('player', 'category', 'season', 'value', 'comment')

    def get_initial(self):
        slug = self.kwargs.get('slug')
        kwargs = super(PlayerStampAddView, self).get_initial()
        if slug is not None:
            player = get_object_or_404(Player, username=slug)
            kwargs['player'] = player
        return kwargs

    def post(self, request, *args, **kwargs):
        return super(PlayerStampAddView, self).post(request, *args, **kwargs)

    def get_success_url(self):
        player = get_object_or_404(Player, username=self.kwargs.get('slug'))
        return reverse_lazy('player:detail', kwargs={'slug': player.username})

    @method_decorator(permission_required('league.add_reward'))
    def dispatch(self, request, *args, **kwargs):
        return super(PlayerStampAddView, self).dispatch(request, *args, **kwargs)


class LeagueListView(ListView):
    model = LeagueSeason
    template_name = "league/leagueseason_list.html"

    def get_context_data(self, **kwargs):
        context = super(LeagueListView, self).get_context_data(**kwargs)
        leagues = self.model.objects.all()
        active = leagues.filter(end_date__gte=now)
        past = leagues.filter(end_date__lte=now).order_by('game__name', '-end_date')
        context.update({'active': active, 'past': past})
        return context


class GivePrizesView(View):

    @method_decorator(permission_required('league.add_leagueenroll'))  # edit/change
    def dispatch(self, request, *args, **kwargs):
        return super(GivePrizesView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        given = [key.replace('user:', '') for key, value in request.POST.iteritems() if key.startswith('user:') and value == 'on']
        result = LeagueEnroll.objects.filter(player__username__in=given, season__slug=self.kwargs['slug']).update(reward_given=True)
        messages.success(request, 'Wydano nagrody. Liczba nagrodzonych: %s' % result)
        return HttpResponseRedirect(reverse_lazy('league:detail', kwargs={'slug': self.kwargs.get('slug')}))

    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('league:detail', kwargs={'slug': self.kwargs.get('slug')}))


class PlayerMatchAddView(FormView):
    template_name = 'league/add_match.html'
    form_class = PlayerMatchForm

    @method_decorator(permission_required('league.add_match'))
    def dispatch(self, request, *args, **kwargs):
        return super(PlayerMatchAddView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        slug = self.kwargs.get('slug')
        if slug is not None:
            player = get_object_or_404(Player, username=slug)
            return reverse_lazy('player:detail', kwargs={'slug': player.username})
        else:
            return reverse_lazy('league_utils:add-match')

    def post(self, request, *args, **kwargs):
        debug = kwargs.get('debug', False)
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            player_1 = form.cleaned_data['player_1']
            player_2 = form.cleaned_data['player_2']
            won_1 = form.cleaned_data['won_1']
            won_2 = form.cleaned_data['won_2']
            if won_1 > won_2:
                winner, loser = player_1, player_2
                won, lost = won_1, won_2
            else:
                winner, loser = player_2, player_1
                won, lost = won_2, won_1
            season = form.cleaned_data['season']
            # category = season.default_match_category
            num = Match.objects.filter(season=season, players=player_1).filter(players=player_2).exclude(ignore=True).distinct().count()
            if num < season.max_matches:
                ignore = False
            else:
                ignore = True

            season.report_match(winner=winner, loser=loser, won=won, lost=lost, when=now,
                                ignore=ignore)
            # message!
            messages.success(request, _('Match successfully added!'))
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class LeagueEnrollView(FormView):
    template_name = 'league/season_enroll.html'
    form_class = SeasonEnrollForm

    @method_decorator(permission_required('league.add_leagueenroll'))
    def dispatch(self, request, *args, **kwargs):
        return super(LeagueEnrollView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        slug = self.kwargs.get('slug')
        if slug is None:
            form_class = self.get_form_class()
            form = self.get_form(form_class)
            if form.is_valid():
                season = form.cleaned_data['season']
                slug = season.slug
            else:
                messages.warning(self.request, _("We've had some problems returning to original page."))
                return reverse_lazy('league:index')
        return reverse_lazy('league:detail', kwargs={'slug': slug})

    def get_initial(self):
        kwargs = super(LeagueEnrollView, self).get_initial()
        slug = self.kwargs.get('slug')
        if slug is not None:
            season = get_object_or_404(LeagueSeason, slug=slug)
            kwargs['season'] = season
        return kwargs

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            season = form.cleaned_data['season']
            for player in form.cleaned_data['players']:
                params = {'player': player.get_full_name(), 'season': season}
                if not season.enroll_player(player):
                    messages.warning(request, _('Player %(player)s not added to season %(season)s.') % params)
                else:
                    messages.success(request, _('Player %(player)s added to season %(season)s.') % params)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class PlayerEnrollView(LeagueEnrollView):
    def get_success_url(self):
        slug = self.kwargs.get('slug')
        return reverse_lazy('player:detail', kwargs={'slug': slug})

    def get_initial(self):
        kwargs = self.initial.copy()
        slug = self.kwargs.get('slug')
        if slug is not None:
            player = get_object_or_404(Player, username=slug)
            kwargs['players'] = [player]
        return kwargs
