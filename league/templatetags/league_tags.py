# coding: utf-8
__author__ = 'minder'

from django import template
from django.db.models import Sum
from django.utils.timezone import now
from django.forms.models import modelformset_factory

from ..models import Match, LeagueSeason, Reward, Game, Player
from ..forms import SeasonEnrollForm
from league.models import LeagueEnroll


register = template.Library()


@register.inclusion_tag('league/tag_lastmatches.html', takes_context=True)
def lastmatches(context, player):
    user = context.get('user')
    match_list = Match.objects.filter(matchresult__player=player).order_by('-when')[:5]
    return {
        'player': player,
        'match_list': match_list,
        'user': user,
    }


@register.inclusion_tag('league/tag_canweplay.html', takes_context=True)
def canweplay(context, player):
    seasons = LeagueSeason.objects.filter(end_date__gte=now, players=player)
    user = context.get('user')
    return {
        'user': user,
        'player': player,
        'season_list': seasons,
    }


@register.assignment_tag
def season_matches(season, player):
    return Match.objects.filter(season=season, players=player).order_by('-when')


@register.assignment_tag
def current_seasons():
    return LeagueSeason.objects.filter(start_date__lte=now, end_date__gte=now)


@register.assignment_tag
def player_seasons(player):
    return LeagueSeason.objects.filter(players__in=[player]).order_by('-start_date')


@register.assignment_tag(takes_context=True)
def times_played_this_season(context, player, opp):
    season = context.get('season') or context.get('leagueseason')
    return season.howmanyplayed(player, opp)


@register.assignment_tag(takes_context=True)
def with_me(context, match):
    user = context['user']
    if user.is_authenticated():
        return match.matchresult_set.filter(player=user).count() == 1
    else:
        return False


@register.assignment_tag
def get_player_seasons(person):
    return LeagueSeason.objects.filter(players=person).order_by('-start_date')


@register.assignment_tag
def get_player_games(person):
    return Game.objects.filter(leagueseason__players=person).distinct()


@register.inclusion_tag('league/tag_link_user.html')
def link_user(person):
    if not isinstance(person, Player):
        try:
            person = Player.objects.get(username=person.get('username'))
        except:
            person = Player.objects.get(username='minder')
    return {
        'person': person
    }


@register.simple_tag
def fullname(person):
    return person.get_full_name() or person.username


@register.simple_tag
def league_stamps(person, game):
    return Reward.objects.filter(season__game=game, player=person).distinct().aggregate(value=Sum('value'))['value']


@register.inclusion_tag('league/tag_season_enroll.html', takes_context=True)
def season_enroll_form(context):
    season = context.get('leagueseason', None)
    form = SeasonEnrollForm()
    form.fields['players'].queryset = Player.objects.exclude(leagueseason=season)
    form.fields['season'].initial = season
    return {'season': season, 'form': form}


@register.assignment_tag
def prize_formset(season):
    PrizeFormset = modelformset_factory(LeagueEnroll, fields=('reward_given',))
    formset = PrizeFormset(queryset=LeagueEnroll.objects.filter(season=season).order_by('player__id'))
    return formset


@register.assignment_tag
def prep_formset(season):
    PrizeFormset = modelformset_factory(LeagueEnroll, fields=('reward_given',), extra=0)
    formset = PrizeFormset(queryset=LeagueEnroll.objects.filter(season=season).order_by('player__id'))
    return dict((form.instance.player.username, form) for form in formset.forms)


@register.assignment_tag
def prep_form(prep, player):
    return prep[player['username']]


@register.assignment_tag
def prep_prize_data(season):
    qs = LeagueEnroll.objects.filter(season=season).order_by('player__id')
    return dict((obj.player.username, obj.reward_given) for obj in qs)
