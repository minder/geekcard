#coding: utf-8
__author__ = 'minder'

from django import forms
import django_select2.fields as s2fields
from django.utils.timezone import now

from .models import Tournament, LeagueSeason, Reward
from account.models import Player



############## FIELDS

class PlayerChoiceField(s2fields.AutoModelSelect2Field):
    queryset = Player.objects
    search_fields = ['first_name__icontains', 'last_name__icontains', 'club_cards__barcode__contains']


class MultiplePlayerChoiceField(s2fields.AutoModelSelect2MultipleField):
    queryset = Player.objects
    search_fields = ['first_name__icontains', 'last_name__icontains', 'club_cards__barcode__contains']


############## FORMS

class TournamentForm(forms.ModelForm):
    result_file = forms.FileField(required=False)

    class Meta:
        model = Tournament

    def save(self, commit=True):
        super(TournamentForm, self).save(commit=False)
        plik = self.cleaned_data.get('result_file')
        if plik:
            self.instance.result = plik.read()
        return super(TournamentForm, self).save(commit=True)


class LeagueSeasonForm(forms.ModelForm):
    class Meta:
        model = LeagueSeason


class PlayerMatchForm(forms.Form):
    season = forms.ModelChoiceField(queryset=LeagueSeason.objects.filter(start_date__lte=now, end_date__gte=now))
    player_1 = PlayerChoiceField()
    player_2 = PlayerChoiceField()
    won_1 = forms.CharField()
    won_2 = forms.CharField()


class RewardForm(forms.ModelForm):
    player = PlayerChoiceField()
    season = forms.ModelChoiceField(queryset=LeagueSeason.objects.filter(start_date__lte=now, end_date__gte=now))

    class Meta:
        model = Reward


class SeasonEnrollForm(forms.Form):
    players = MultiplePlayerChoiceField()
    season = forms.ModelChoiceField(queryset=LeagueSeason.objects.filter(start_date__lte=now, end_date__gte=now))


class GivePrizesForm(forms.Form):
    players = MultiplePlayerChoiceField()
    season = forms.ModelChoiceField(queryset=LeagueSeason.objects.filter(end_date__lte=now))
