#coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (Game, GameID, LeagueSeason, EventCategory,
                     Match, MatchResult, Reward, RewardCategory, Tournament,
                     PromoItem, LeagueEnroll)
from . import utils
from .forms import TournamentForm, LeagueSeasonForm


class MyAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    save_on_top = True


class GameAdmin(MyAdmin):
    list_display = ['name', ]
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Game, GameAdmin)


class GameIDInline(admin.StackedInline):
    model = GameID

    def get_max_num(self, request, obj=None, **kwargs):
        return Game.objects.all().count()


class GameIDAdmin(MyAdmin):
    list_display = ['player', 'number', 'game']
    search_fields = ['player__last_name', 'player__first_name', 'number', ]
    ordering = ['player__last_name']


admin.site.register(GameID, GameIDAdmin)


class LeagueEnrollInline(admin.StackedInline):
    model = LeagueEnroll


class LeagueSeasonAdmin(MyAdmin):
    form = LeagueSeasonForm
    date_hierarchy = 'start_date'
    filter_horizontal = ['players', ]
    list_display = ['name', 'game', 'start_date', 'end_date', 'number_of_players']
    list_filter = ['game']
    prepopulated_fields = {'slug': ('name',)}
    ordering = ['-start_date']
    inlines = [LeagueEnrollInline,]

    def number_of_players(obj):
        return obj.players.all().count()

    number_of_players.short_description = _('Players')
    number_of_players = staticmethod(number_of_players)


admin.site.register(LeagueSeason, LeagueSeasonAdmin)


class EventCategoryAdmin(MyAdmin):
    list_display = ['name', 'reward_multiplier', 'max_players']


admin.site.register(EventCategory, EventCategoryAdmin)


class MatchResultInline(admin.TabularInline):
    model = MatchResult

    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.category:
            return obj.category.max_players - obj.matchresult_set.count()
        return 2

    def get_max_num(self, request, obj=None, **kwargs):
        if obj and obj.category:
            return obj.category.max_players
        return 8


class MatchAdmin(MyAdmin):
    date_hierarchy = 'when'
    list_filter = ['category', 'season', 'ignore']
    list_display = ['when', 'category', 'season', 'ignore']
    inlines = [
        MatchResultInline,
    ]

    def save_related(self, request, form, formsets, change):
        form.save_m2m()
        for formset in formsets:
            self.save_formset(request, form, formset, change=change)

        match = form.instance
        utils.update_results(match)


admin.site.register(Match, MatchAdmin)


class MatchResultAdmin(MyAdmin):
    list_display = ['match', 'player', 'games_won', 'points']
    search_fields = ['player__last_name', 'player__gameid__number']


admin.site.register(MatchResult, MatchResultAdmin)


class RewardCategoryAdmin(MyAdmin):
    list_display = ['name', 'value']


admin.site.register(RewardCategory, RewardCategoryAdmin)


class RewardAdmin(MyAdmin):
    list_display = ['comment', 'player', 'when']
    list_filter = ['category']
    search_fields = ['player__last_name', 'player__club_cards__barcode', 'player__gameid__number', 'category__name']
    date_hierarchy = 'when'
    ordering = ['-when']
    fieldsets = (
        (None, {
            'fields': ('player', 'season', 'category', ),
        }),
        (_('Extra Options'), {
            'fields': ('when', 'value', 'comment'),
            'classes': ('collapse',),
        }),
    )


admin.site.register(Reward, RewardAdmin)


class TournamentAdmin(MyAdmin):
    list_display = ['get_name', 'has_result', 'start_date', 'end_date']
    date_hierarchy = 'start_date'
    form = TournamentForm
    fieldsets = (
        (None, {
            'fields': ('season', 'category', 'result_file',),
        }),
        (_('Extra Options'), {
            'fields': ('name', 'start_date', 'end_date'),
            'classes': ('collapse',),
        }),
    )

    def has_result(obj):
        return bool(obj.result)

    has_result.short_description = _('has results')
    has_result.boolean = True
    has_result = staticmethod(has_result)

    def get_name(obj):
        return obj.__unicode__()

    get_name.short_description = _('name')
    get_name = staticmethod(get_name)


admin.site.register(Tournament, TournamentAdmin)


class PromoItemAdmin(MyAdmin):
    list_display = ['name', 'get_pic', 'reward_category']

    def get_pic(obj):
        return '<img src="%s" width="100" alt="%s">' % (obj.image, obj.name)

    get_pic.allow_tags = True
    get_pic.short_description = _('picture')
    get_pic = staticmethod(get_pic)


admin.site.register(PromoItem, PromoItemAdmin)

