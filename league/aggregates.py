#coding: utf-8
from django.db.models.sql.aggregates import Aggregate
from django.db.models import Aggregate as Ag


class SumWithDefaultSQL(Aggregate):
    is_ordinal = True

    def __init__(self, col, default=None, **extra):
        super(SumWithDefaultSQL, self).__init__(col, default=default, **extra)
        self.sql_function = 'SUM'
        if default is not None:
            self.sql_template = 'COALESCE(%(function)s(%(field)s), %(default)s)'


class SumWithDefault(Ag):
    name = 'Sum'

    def add_to_query(self, query, alias, col, source, is_summary):
        aggregate = SumWithDefaultSQL(col, source=source, is_summary=is_summary, **self.extra)
        query.aggregates[alias] = aggregate
