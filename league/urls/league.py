#coding: utf-8
__author__ = 'minder'

from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, ListView, DetailView
from ..views import LeagueListView, PlayerMatchAddView, LeagueEnrollView, GivePrizesView
from ..models import LeagueSeason

urlpatterns = patterns(
    '',
    # index powinien wyświetlać zestawienie wszystkich lig
    url(r'^$', LeagueListView.as_view(), name='index'),
    url(r'^(?P<slug>[\w-]+)/$', DetailView.as_view(model=LeagueSeason), name='detail'),
    url(r'^(?P<slug>[\w-]+)/enroll/$', LeagueEnrollView.as_view(), name='enroll'),
    url(r'^(?P<slug>[\w-]+)/give_prizes/$', GivePrizesView.as_view(), name='give_prizes'),
)
