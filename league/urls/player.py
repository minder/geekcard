#coding: utf-8
__author__ = 'minder'

from lending.views import ShowCart

from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, ListView, DetailView, ArchiveIndexView
from ..models import Player
from ..views import (PlayerMatchHistoryView, PlayerMatchAddView,
                     PlayerStampHistoryView, PlayerStampAddView,
                     my_profile_view, PlayerEnrollView,
                     )

urlpatterns = patterns(
    '',
    url(r'^my_profile/$', my_profile_view, name='profile'),
    url(r'^$', TemplateView.as_view(template_name='account/player_list.html'), name='index'),
    url(r'^(?P<slug>[\w.@-]+)/$', DetailView.as_view(model=Player, slug_field='username'), name='detail'),
    url(r'^(?P<slug>[\w.@-]+)/enroll/$', PlayerEnrollView.as_view(), name='enroll'),
    url(r'^(?P<slug>[\w.@-]+)/matches/$', PlayerMatchHistoryView.as_view(), name='match_history'),
    url(r'^(?P<slug>[\w.@-]+)/matches/add/$', PlayerMatchAddView.as_view(), name='match_add'),
    url(r'^(?P<slug>[\w.@-]+)/stamps/$', PlayerStampHistoryView.as_view(), name='stamp_history'),
    url(r'^(?P<slug>[\w.@-]+)/stamps/add/$', PlayerStampAddView.as_view(), name='stamp_add'),
    url(r'^(?P<slug>[\w.@-]+)/cart/$', ShowCart.as_view(), name='show_cart'),
)

