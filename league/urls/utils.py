# coding: utf-8
from django.contrib.auth.decorators import user_passes_test
from account.views import RegisterPlayerView
from lending.models import Cart

__author__ = 'minder'

from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, ListView, DetailView
from ..views import LeagueListView, PlayerMatchAddView, LeagueEnrollView, PlayerStampAddView
from ..models import LeagueSeason

urlpatterns = patterns(
    '',
    # index powinien wyświetlać dostępne narzędzia ligowe
    # url(r'^$', LeagueListView.as_view(), name='index'),
    url(r'^enroll/$', LeagueEnrollView.as_view(), name='enroll'),
    url(r'^add-stamps/$', PlayerStampAddView.as_view(), name='add-stamps'),
    url(r'^add-match/$', PlayerMatchAddView.as_view(), name='add-match'),
    url(r'^add-player/$', RegisterPlayerView.as_view(), name='add-player'),
    url(r'^list-carts/$', ListView.as_view(model=Cart), name='list-carts'),
)
