#coding: utf-8
__author__ = 'minder'
from django.db.models import Max
import datetime


def update_results(match):
    best = match.matchresult_set.filter(games_won=match.matchresult_set.aggregate(max_won=Max('games_won'))['max_won'])
    results = match.matchresult_set.all()
    if best.count() == results.count():
        # remis
        results.update(points=match.season.game.points_for_draw)
    else:
        # ktoś wygrał
        results.exclude(pk__in=best).update(points=match.season.game.points_for_losing)
        best.update(points=match.season.game.points_for_winning)


def four_hours_from_now():
    return datetime.datetime.now()+datetime.timedelta(hours=4)
