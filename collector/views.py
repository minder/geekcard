#coding: utf-8
from django.shortcuts import render

from django.views.generic import TemplateView, ListView, DetailView
from .models import MTGExpansion, MTGCard, MTGCardBase


class MTGExpansionList(ListView):
    model = MTGExpansion


class MTGExpansionCardList(ListView):
    model = MTGCard

    def get_queryset(self):
        slug = self.kwargs.get('slug')
        return super(MTGExpansionCardList, self).get_queryset().filter(expansion__abbrev__iexact=slug)

    def get_context_data(self, **kwargs):
        context = super(MTGExpansionCardList, self).get_context_data(**kwargs)
        slug = self.kwargs.get('slug')
        exp = MTGExpansion.objects.get(abbrev__iexact=slug)
        context.update({'expansion': exp})
        return context

class MTBaseCardView(DetailView):
    model = MTGCardBase


class MTGCardView(DetailView):
    model = MTGCard

