# coding: utf-8
from collector.utils import get_photo_url
from django.conf import settings
from mtglib.gatherer_request import SearchRequest
from mtglib.card_extractor import CardExtractor
from .models import MTGExpansion, MTGCardBase, MTGCard, MTGRarity, MTGCardType, MTGCardSubtype
import requests
from cStringIO import StringIO
from PIL import Image, ImageDraw
import flickr_api

flickr_api.set_keys(api_key=settings.FLICKR_API['KEY'], api_secret=settings.FLICKR_API['SECRET'])
flickr_api.set_auth_handler(settings.FLICKR_API['AUTH_HANDLER'])

GATHERER_IMG_URL_TEMPLATE = 'http://gatherer.wizards.com/Handlers/Image.ashx?type=card&multiverseid=%s'

expansions = {
    'Alara Reborn': 'ARB',
    'Alliances': 'ALL',
    'Antiquities': 'ATQ',
    'Apocalypse': 'APC',
    'Arabian Nights': 'ARN',
    'Archenemy': 'ARC',
    'Avacyn Restored': 'AVR',
    'Battle Royale Box Set': 'BRY',
    'Beatdown Box Set': 'BTD',
    'Betrayers of Kamigawa': 'BOK',
    'Born of the Gods': 'BNG',
    'Champions of Kamigawa': 'CHK',
    'Chronicles': 'CHR',
    'Classic Sixth Edition': '6ED',
    'Coldsnap': 'CSP',
    'Commander 2013 Edition': 'C13',
    "Commander's Arsenal": 'CM1',
    'Conflux': 'CON',
    # u'Magic: The Gathering\u2014Conspiracy': 'CNS',
    'Dark Ascension': 'DKA',
    'Darksteel': 'DST',
    'Dissension': 'DIS',
    "Dragon's Maze": 'DGM',
    'Duel Decks: Ajani vs. Nicol Bolas': 'DDH',
    'Duel Decks: Divine vs. Demonic': 'DDC',
    'Duel Decks: Elspeth vs. Tezzeret': 'DDF',
    'Duel Decks: Elves vs. Goblins': 'EVG',
    'Duel Decks: Garruk vs. Liliana': 'DDD',
    'Duel Decks: Heroes vs. Monsters': 'DDL',
    'Duel Decks: Izzet vs. Golgari': 'DDJ',
    'Duel Decks: Jace vs. Chandra': 'DD2',
    'Duel Decks: Jace vs. Vraska': 'JVV',
    'Duel Decks: Knights vs. Dragons': 'DDG',
    'Duel Decks: Phyrexia vs. the Coalition': 'DDE',
    'Duel Decks: Sorin vs. Tibalt': 'DDK',
    'Duel Decks: Venser vs. Koth': 'DDI',
    'Eighth Edition': '8ED',
    'Eventide': 'EVE',
    'Exodus': 'EXO',
    'Fallen Empires': 'FEM',
    'Fifth Dawn': '5DN',
    'Fifth Edition': '5ED',
    'Fourth Edition': '4ED',
    'From the Vault: Dragons': 'DRB',
    'From the Vault: Exiled': 'V09',
    'From the Vault: Legends': 'V11',
    'From the Vault: Realms': 'V12',
    'From the Vault: Relics': 'V10',
    'From the Vault: Twenty': 'V13',
    'Future Sight': 'FUT',
    'Gatecrash': 'GTC',
    'Guildpact': 'GPT',
    'Homelands': 'HML',
    'Ice Age': 'ICE',
    'Innistrad': 'ISD',
    'Invasion': 'INV',
    'Journey into Nyx': 'JOU',
    'Judgment': 'JUD',
    'Legends': 'LEG',
    'Legions': 'LGN',
    'Limited Edition Alpha': 'LEA',
    'Limited Edition Beta': 'LEB',
    'Lorwyn': 'LRW',
    'Magic 2010': 'M10',
    'Magic 2011': 'M11',
    'Magic 2012': 'M12',
    'Magic 2013': 'M13',
    'Magic 2014 Core Set': 'M14',
    'Magic 2015 Core Set': 'M15',
    'Magic: The Gathering-Commander': 'CMD',
    'Mercadian Masques': 'MMQ',
    'Mirage': 'MIR',
    'Mirrodin': 'MRD',
    'Mirrodin Besieged': 'MBS',
    "Modern Event Deck 2014": 'MD1',
    'Modern Masters': 'MMA',
    'Morningtide': 'MOR',
    'Nemesis': 'NEM',
    'New Phyrexia': 'NPH',
    'Ninth Edition': '9ED',
    'Odyssey': 'ODY',
    'Onslaught': 'ONS',
    'Planar Chaos': 'PLC',
    'Planechase': 'HOP',
    'Planechase 2012 Edition': 'PC2',
    'Planeshift': 'PLS',
    'Portal': 'POR',
    'Portal Second Age': 'P02',
    'Portal Three Kingdoms': 'PTK',
    'Premium Deck Series: Fire and Lightning': 'PD2',
    'Premium Deck Series: Graveborn': 'PD3',
    'Premium Deck Series: Slivers': 'H09',
    'Prophecy': 'PCY',
    'Ravnica: City of Guilds': 'RAV',
    'Return to Ravnica': 'RTR',
    'Revised Edition': '3ED',
    'Rise of the Eldrazi': 'ROE',
    'Saviors of Kamigawa': 'SOK',
    'Scars of Mirrodin': 'SOM',
    'Scourge': 'SCG',
    'Seventh Edition': '7ED',
    'Shadowmoor': 'SHM',
    'Shards of Alara': 'ALA',
    'Starter 1999': 'S99',
    'Starter 2000': 'S00',
    'Stronghold': 'STH',
    'Tempest': 'TMP',
    'Tenth Edition': '10E',
    'The Dark': 'DRK',
    'Theros': 'THS',
    'Time Spiral': 'TSP',
    'Time Spiral "Timeshifted"': 'TSB',
    'Torment': 'TOR',
    #'Unglued': 'UGL',  # TODO
    #'Unhinged': 'UNH', # TODO
    'Unlimited Edition': '2ED',
    "Urza's Destiny": 'UDS',
    "Urza's Legacy": 'ULG',
    "Urza's Saga": 'USG',
    'Visions': 'VIS',
    'Weatherlight': 'WTH',
    'Worldwake': 'WWK',
    'Zendikar': 'ZEN'
}

# MTG_RARITY = {
#     'Token': -2,
#     'Proxy': -1,
#     'Land': 0,
#     'Common': 1,
#     'Uncommon': 2,
#     'Rare': 3,
#     'Mythic Rare': 4,
#     "Special": 5,
#     "Promo": 6,
# }

rev_sets = dict((val, key) for (key, val) in expansions.iteritems())
# rev_sets = {val: key for key, val in expansions.iteritems()}


def import_gatherer(expansion=None):
    if not expansion:
        for e in expansions:
            import_gatherer_set(e)
    elif expansion in expansions.keys():
        import_gatherer_set(expansion)


def flickr_get_or_upload(gatherer_id, card, photoset, photos):
    gatherer_id = str(gatherer_id)
    orig_img_url = GATHERER_IMG_URL_TEMPLATE % gatherer_id

    if gatherer_id in photos:
        return get_photo_url(photos[gatherer_id])

    try:
        r = requests.get(orig_img_url)
        img = StringIO(r.content)
        # img.seek(0)
        photo = flickr_api.upload(img, title=gatherer_id, tags=u'"%s"' % card.name)
        photoset.addPhoto(photo_id=photo.id)
        return get_photo_url(photo)
    except Exception as exc:
        return orig_img_url


def import_gatherer_set(exp_name):
    if exp_name not in expansions:
        if exp_name.upper() in rev_sets:
            exp_name = rev_sets[exp_name.upper()]
        else:
            raise NotImplementedError('Set does not exist or is not implemented.')
    print "Importing %s..." % exp_name

    flickr_user = flickr_api.test.login()
    # photosets = {ps.title.replace('MTG_', ''): ps for ps in flickr_user.getPhotosets().data if ps.title.startswith('MTG_')}
    photosets = dict((ps.title.replace('MTG_', ''), ps) for ps in flickr_user.getPhotosets().data if ps.title.startswith('MTG_'))
    photoset = photosets.get(expansions[exp_name], None)

    exp, created = MTGExpansion.objects.get_or_create(name=exp_name, abbrev=expansions[exp_name])

    if photoset is not None:
        # photos = {p.title: p for p in photoset.getPhotos().data}
        photos = dict((p.title, p) for p in photoset.getPhotos().data)
    else:
        # create temporary cover
        im = Image.new('RGB', (50, 30))
        draw = ImageDraw.Draw(im)
        col = (200, 200, 200)  # light gray
        text_pos = (10, 10)
        draw.text(text_pos, exp.abbrev, fill=col)
        img = StringIO()
        im.save(img, 'PNG')
        img.seek(0)
        del draw
        photo = flickr_api.upload(img, title='cover')
        photoset = flickr_api.Photoset.create(title='MTG_%s' % exp.abbrev, primary_photo_id=photo.id)
        photos = {}

    if isinstance(exp_name, unicode):
        exp_name = exp_name.encode('utf-8')
    request = SearchRequest({'set': exp_name}, special=True)
    cards = []
    oldcards = []
    for i in range(20):
        print "?",
        tmp = CardExtractor(request.url + "&page=%s" % i).cards
        if len(tmp) == 0 or (len(oldcards) > 0 and tmp[0].name == oldcards[0].name):
            break
        else:
            cards.extend(tmp)
            oldcards = tmp
    print ''

    for card in cards:
        printings = card.printings_full

        for version in printings.get(exp_name, []):
            rarity = version['rarity']
            gatherer_id = version['id']
            rarobj, created = MTGRarity.objects.get_or_create(name=rarity)
            card_name = card.name.encode('utf-8') if isinstance(card.name, unicode) else card.name
            try:
                k = MTGCardBase.objects.get(name=card_name)
                print "!",
            except MTGCardBase.DoesNotExist:
                print "+",
                k = MTGCardBase.objects.create(name=card_name, oracle_rules=card.rules_text, power=card.power,
                                               toughness=card.toughness, loyalty=card.loyalty, mana_cost=card.mana_cost)
                for main_types in card.types:
                    obj, created = MTGCardType.objects.get_or_create(name=main_types)
                    k.card_type.add(obj)
                for sub_type in card.subtypes:
                    obj, created = MTGCardSubtype.objects.get_or_create(name=sub_type)
                    k.card_subtype.add(obj)
                k.save()
            try:
                karta = MTGCard.objects.get(base=k, expansion=exp, gatherer_id=gatherer_id)
                if not karta.artwork or 'gatherer.wizards.com' in karta.artwork:
                    karta.artwork = flickr_get_or_upload(gatherer_id=gatherer_id, card=card, photoset=photoset, photos=photos)
                    karta.save()
                    print "I",  # image swap
                else:
                    print ".",
            except MTGCard.DoesNotExist:
                photo_url = flickr_get_or_upload(gatherer_id=gatherer_id, card=card, photoset=photoset, photos=photos)
                karta = MTGCard.objects.create(base=k, expansion=exp, rarity=rarobj, name=card_name,
                                               gatherer_id=gatherer_id, artwork=photo_url)

                print "@",
    print ""
