# coding: utf-8
from django.contrib import admin

from .models import (MTGCardBase, MTGCard, MTGExpansion, MTGBlock, MTGCardType, MTGCardSubtype,
                     MTGArchetype, MTGDeck, MTGFormat, MTGRarity, MTGOwnedCard)

### Actions definitions

def mark_special(modeladmin, request, queryset):
    queryset.update(is_special=True)
mark_special.short_description = "Oznacz jako edycja specjalna"


def mark_core(modeladmin, request, queryset):
    queryset.update(is_core=True)
mark_core.short_description = "Oznacz jako Core Set"


### Admin classes

class MyAdmin(admin.ModelAdmin):
    search_fields = ['name']
    actions_on_bottom = True
    save_on_top = True


class MTGCardAdmin(MyAdmin):
    list_display = ['name', 'expansion', 'rarity', 'artist']
    list_filter = ['rarity']

admin.site.register(MTGCard, MTGCardAdmin)


class MTGExpansionAdmin(MyAdmin):
    list_display = ['name', 'abbrev', 'release_date', 'block', 'is_core', 'is_special']
    list_filter = ['is_core', 'is_special']
    list_editable = ['release_date']
    actions = [mark_special, mark_core]

admin.site.register(MTGExpansion, MTGExpansionAdmin)


admin.site.register(
    (MTGCardBase, MTGBlock, MTGCardType, MTGCardSubtype,
     MTGArchetype, MTGDeck, MTGFormat, MTGRarity), MyAdmin
)

admin.site.register(MTGOwnedCard)