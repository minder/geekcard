# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MTGFormat'
        db.create_table(u'collector_mtgformat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('badge_color', self.gf('django.db.models.fields.CharField')(max_length=7, blank=True)),
        ))
        db.send_create_signal(u'collector', ['MTGFormat'])

        # Adding field 'MTGDeck.in_progress'
        db.add_column(u'collector_mtgdeck', 'in_progress',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'MTGDeck.format'
        db.add_column(u'collector_mtgdeck', 'format',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGFormat'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'MTGFormat'
        db.delete_table(u'collector_mtgformat')

        # Deleting field 'MTGDeck.in_progress'
        db.delete_column(u'collector_mtgdeck', 'in_progress')

        # Deleting field 'MTGDeck.format'
        db.delete_column(u'collector_mtgdeck', 'format_id')


    models = {
        u'account.player': {
            'Meta': {'ordering': "[u'last_name']", 'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'collector.artist': {
            'Meta': {'object_name': 'Artist'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'collector.language': {
            'Meta': {'object_name': 'Language'},
            'abbrev': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'collector.mtgarchetype': {
            'Meta': {'object_name': 'MTGArchetype'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'collector.mtgblock': {
            'Meta': {'object_name': 'MTGBlock'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'collector.mtgcard': {
            'Meta': {'object_name': 'MTGCard'},
            'artist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.Artist']", 'null': 'True', 'blank': 'True'}),
            'artwork': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'base': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'expansion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']"}),
            'flavor_text': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.Language']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'rarity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGRarity']"}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'})
        },
        u'collector.mtgcardbase': {
            'Meta': {'object_name': 'MTGCardBase'},
            'card_subtype': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['collector.MTGCardSubtype']", 'null': 'True', 'blank': 'True'}),
            'card_type': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['collector.MTGCardType']", 'null': 'True', 'blank': 'True'}),
            'cmc': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loyalty': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'}),
            'mana_cost': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'oracle_rules': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'power': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'}),
            'toughness': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'})
        },
        u'collector.mtgcardsubtype': {
            'Meta': {'object_name': 'MTGCardSubtype'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'collector.mtgcardtype': {
            'Meta': {'object_name': 'MTGCardType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'collector.mtgdeck': {
            'Meta': {'object_name': 'MTGDeck'},
            'archetypes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['collector.MTGArchetype']", 'symmetrical': 'False'}),
            'format': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGFormat']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_progress': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mainboard': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'in_mainboard'", 'symmetrical': 'False', 'through': u"orm['collector.MTGDeckMainCard']", 'to': u"orm['collector.MTGCardBase']"}),
            'maybeboard': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'in_maybeboard'", 'symmetrical': 'False', 'through': u"orm['collector.MTGDeckMaybeCard']", 'to': u"orm['collector.MTGCardBase']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sideboard': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'in_sideboard'", 'symmetrical': 'False', 'through': u"orm['collector.MTGDeckSideCard']", 'to': u"orm['collector.MTGCardBase']"})
        },
        u'collector.mtgdeckmaincard': {
            'Meta': {'object_name': 'MTGDeckMainCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'deck': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGDeck']"}),
            'exp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'collector.mtgdeckmaybecard': {
            'Meta': {'object_name': 'MTGDeckMaybeCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'deck': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGDeck']"}),
            'exp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'collector.mtgdecksidecard': {
            'Meta': {'object_name': 'MTGDeckSideCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'deck': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGDeck']"}),
            'exp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'collector.mtgexpansion': {
            'Meta': {'object_name': 'MTGExpansion'},
            'abbrev': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGBlock']", 'null': 'True', 'blank': 'True'}),
            'cards_total': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'release_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'symbol': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'collector.mtgformat': {
            'Meta': {'object_name': 'MTGFormat'},
            'badge_color': ('django.db.models.fields.CharField', [], {'max_length': '7', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        u'collector.mtgownedcard': {
            'Meta': {'object_name': 'MTGOwnedCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCard']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'condition': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'foil': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'signed': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'collector.mtgrarity': {
            'Meta': {'object_name': 'MTGRarity'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['collector']