# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MTGBlock'
        db.create_table(u'collector_mtgblock', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'collector', ['MTGBlock'])

        # Adding model 'MTGExpansion'
        db.create_table(u'collector_mtgexpansion', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('abbrev', self.gf('django.db.models.fields.CharField')(max_length=4)),
            ('symbol', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('release_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGBlock'], null=True, blank=True)),
            ('cards_total', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'collector', ['MTGExpansion'])

        # Adding model 'MTGRarity'
        db.create_table(u'collector_mtgrarity', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=7)),
        ))
        db.send_create_signal(u'collector', ['MTGRarity'])

        # Adding model 'MTGCardType'
        db.create_table(u'collector_mtgcardtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'collector', ['MTGCardType'])

        # Adding model 'MTGCardSubtype'
        db.create_table(u'collector_mtgcardsubtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'collector', ['MTGCardSubtype'])

        # Adding model 'MTGCardBase'
        db.create_table(u'collector_mtgcardbase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('oracle_rules', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('power', self.gf('django.db.models.fields.CharField')(max_length=5, blank=True)),
            ('toughness', self.gf('django.db.models.fields.CharField')(max_length=5, blank=True)),
            ('loyalty', self.gf('django.db.models.fields.CharField')(max_length=5, blank=True)),
            ('mana_cost', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('cmc', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'collector', ['MTGCardBase'])

        # Adding M2M table for field card_type on 'MTGCardBase'
        m2m_table_name = db.shorten_name(u'collector_mtgcardbase_card_type')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mtgcardbase', models.ForeignKey(orm[u'collector.mtgcardbase'], null=False)),
            ('mtgcardtype', models.ForeignKey(orm[u'collector.mtgcardtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mtgcardbase_id', 'mtgcardtype_id'])

        # Adding M2M table for field card_subtype on 'MTGCardBase'
        m2m_table_name = db.shorten_name(u'collector_mtgcardbase_card_subtype')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mtgcardbase', models.ForeignKey(orm[u'collector.mtgcardbase'], null=False)),
            ('mtgcardsubtype', models.ForeignKey(orm[u'collector.mtgcardsubtype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mtgcardbase_id', 'mtgcardsubtype_id'])

        # Adding model 'Artist'
        db.create_table(u'collector_artist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
        ))
        db.send_create_signal(u'collector', ['Artist'])

        # Adding model 'Language'
        db.create_table(u'collector_language', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('abbrev', self.gf('django.db.models.fields.CharField')(max_length=5)),
        ))
        db.send_create_signal(u'collector', ['Language'])

        # Adding model 'MTGCard'
        db.create_table(u'collector_mtgcard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('language', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.Language'], null=True, blank=True)),
            ('base', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGCardBase'])),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('artwork', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('artist', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.Artist'], null=True, blank=True)),
            ('rarity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGRarity'])),
            ('expansion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGExpansion'])),
            ('flavor_text', self.gf('django.db.models.fields.TextField')()),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'collector', ['MTGCard'])

        # Adding model 'MTGOwnedCard'
        db.create_table(u'collector_mtgownedcard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGCard'])),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Player'])),
            ('condition', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('foil', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('signed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'collector', ['MTGOwnedCard'])

        # Adding model 'MTGArchetype'
        db.create_table(u'collector_mtgarchetype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'collector', ['MTGArchetype'])

        # Adding model 'MTGDeckMainCard'
        db.create_table(u'collector_mtgdeckmaincard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deck', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGDeck'])),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGCardBase'])),
            ('exp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGExpansion'], null=True, blank=True)),
            ('quantity', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'collector', ['MTGDeckMainCard'])

        # Adding model 'MTGDeckSideCard'
        db.create_table(u'collector_mtgdecksidecard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deck', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGDeck'])),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGCardBase'])),
            ('exp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGExpansion'], null=True, blank=True)),
            ('quantity', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'collector', ['MTGDeckSideCard'])

        # Adding model 'MTGDeckMaybeCard'
        db.create_table(u'collector_mtgdeckmaybecard', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deck', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGDeck'])),
            ('card', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGCardBase'])),
            ('exp', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['collector.MTGExpansion'], null=True, blank=True)),
            ('quantity', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'collector', ['MTGDeckMaybeCard'])

        # Adding model 'MTGDeck'
        db.create_table(u'collector_mtgdeck', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'collector', ['MTGDeck'])

        # Adding M2M table for field archetypes on 'MTGDeck'
        m2m_table_name = db.shorten_name(u'collector_mtgdeck_archetypes')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mtgdeck', models.ForeignKey(orm[u'collector.mtgdeck'], null=False)),
            ('mtgarchetype', models.ForeignKey(orm[u'collector.mtgarchetype'], null=False))
        ))
        db.create_unique(m2m_table_name, ['mtgdeck_id', 'mtgarchetype_id'])


    def backwards(self, orm):
        # Deleting model 'MTGBlock'
        db.delete_table(u'collector_mtgblock')

        # Deleting model 'MTGExpansion'
        db.delete_table(u'collector_mtgexpansion')

        # Deleting model 'MTGRarity'
        db.delete_table(u'collector_mtgrarity')

        # Deleting model 'MTGCardType'
        db.delete_table(u'collector_mtgcardtype')

        # Deleting model 'MTGCardSubtype'
        db.delete_table(u'collector_mtgcardsubtype')

        # Deleting model 'MTGCardBase'
        db.delete_table(u'collector_mtgcardbase')

        # Removing M2M table for field card_type on 'MTGCardBase'
        db.delete_table(db.shorten_name(u'collector_mtgcardbase_card_type'))

        # Removing M2M table for field card_subtype on 'MTGCardBase'
        db.delete_table(db.shorten_name(u'collector_mtgcardbase_card_subtype'))

        # Deleting model 'Artist'
        db.delete_table(u'collector_artist')

        # Deleting model 'Language'
        db.delete_table(u'collector_language')

        # Deleting model 'MTGCard'
        db.delete_table(u'collector_mtgcard')

        # Deleting model 'MTGOwnedCard'
        db.delete_table(u'collector_mtgownedcard')

        # Deleting model 'MTGArchetype'
        db.delete_table(u'collector_mtgarchetype')

        # Deleting model 'MTGDeckMainCard'
        db.delete_table(u'collector_mtgdeckmaincard')

        # Deleting model 'MTGDeckSideCard'
        db.delete_table(u'collector_mtgdecksidecard')

        # Deleting model 'MTGDeckMaybeCard'
        db.delete_table(u'collector_mtgdeckmaybecard')

        # Deleting model 'MTGDeck'
        db.delete_table(u'collector_mtgdeck')

        # Removing M2M table for field archetypes on 'MTGDeck'
        db.delete_table(db.shorten_name(u'collector_mtgdeck_archetypes'))


    models = {
        u'account.player': {
            'Meta': {'ordering': "[u'last_name']", 'object_name': 'Player'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'db_index': 'True', 'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'collector.artist': {
            'Meta': {'object_name': 'Artist'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'collector.language': {
            'Meta': {'object_name': 'Language'},
            'abbrev': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'collector.mtgarchetype': {
            'Meta': {'object_name': 'MTGArchetype'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'collector.mtgblock': {
            'Meta': {'object_name': 'MTGBlock'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'collector.mtgcard': {
            'Meta': {'object_name': 'MTGCard'},
            'artist': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.Artist']", 'null': 'True', 'blank': 'True'}),
            'artwork': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'base': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'expansion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']"}),
            'flavor_text': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.Language']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'rarity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGRarity']"}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'})
        },
        u'collector.mtgcardbase': {
            'Meta': {'object_name': 'MTGCardBase'},
            'card_subtype': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['collector.MTGCardSubtype']", 'null': 'True', 'blank': 'True'}),
            'card_type': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['collector.MTGCardType']", 'null': 'True', 'blank': 'True'}),
            'cmc': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'loyalty': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'}),
            'mana_cost': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'oracle_rules': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'power': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'}),
            'toughness': ('django.db.models.fields.CharField', [], {'max_length': '5', 'blank': 'True'})
        },
        u'collector.mtgcardsubtype': {
            'Meta': {'object_name': 'MTGCardSubtype'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'collector.mtgcardtype': {
            'Meta': {'object_name': 'MTGCardType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'collector.mtgdeck': {
            'Meta': {'object_name': 'MTGDeck'},
            'archetypes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['collector.MTGArchetype']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mainboard': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'in_mainboard'", 'symmetrical': 'False', 'through': u"orm['collector.MTGDeckMainCard']", 'to': u"orm['collector.MTGCardBase']"}),
            'maybeboard': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'in_maybeboard'", 'symmetrical': 'False', 'through': u"orm['collector.MTGDeckMaybeCard']", 'to': u"orm['collector.MTGCardBase']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'sideboard': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'in_sideboard'", 'symmetrical': 'False', 'through': u"orm['collector.MTGDeckSideCard']", 'to': u"orm['collector.MTGCardBase']"})
        },
        u'collector.mtgdeckmaincard': {
            'Meta': {'object_name': 'MTGDeckMainCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'deck': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGDeck']"}),
            'exp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'collector.mtgdeckmaybecard': {
            'Meta': {'object_name': 'MTGDeckMaybeCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'deck': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGDeck']"}),
            'exp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'collector.mtgdecksidecard': {
            'Meta': {'object_name': 'MTGDeckSideCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCardBase']"}),
            'deck': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGDeck']"}),
            'exp': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGExpansion']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'collector.mtgexpansion': {
            'Meta': {'object_name': 'MTGExpansion'},
            'abbrev': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGBlock']", 'null': 'True', 'blank': 'True'}),
            'cards_total': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'release_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'symbol': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'})
        },
        u'collector.mtgownedcard': {
            'Meta': {'object_name': 'MTGOwnedCard'},
            'card': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['collector.MTGCard']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'condition': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'foil': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Player']"}),
            'signed': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'collector.mtgrarity': {
            'Meta': {'object_name': 'MTGRarity'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['collector']