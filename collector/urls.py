# coding: utf-8
from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView, ListView, DetailView
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from .views import MTGExpansionCardList, MTGExpansionList, MTBaseCardView, MTGCardView
from .models import Artist

urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='collector/index.html'), name='index'),
    url(r'^game/mtg/$', MTGExpansionList.as_view(), name='mtg_index'),
    url(r'^game/mtg/exp/(?P<slug>[\w-]+)/$', MTGExpansionCardList.as_view(), name='mtg_exp'),
    url(r'^game/mtg/exp/(?P<slug>[\w-]+)/(?P<pk>[\d]+)/$', MTGCardView.as_view(), name='mtg_card'),
    # url(r'^game/mtg/type/$', )
    # url(r'^game/mtg/subtype/$', )
    url(r'^artist/$', ListView.as_view(model=Artist), name='artist_list'),
    url(r'^artist/(?P<pk>[\d]+)/$', DetailView.as_view(model=Artist), name='artist_detail'),
    # url(r'^game/mtg/bcard/(?P<pk>[\d]+)/$', MTBaseCardView.as_view(), name='mtg_bcard'),
    # deck
    # game
    # card
    # mine

)
