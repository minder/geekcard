# coding: utf-8
__author__ = 'minder'

from rest_framework import viewsets
from .models import (MTGCard, MTGExpansion, MTGDeck, MTGCardBase, MTGArchetype, MTGBlock,
                     MTGCardSubtype, MTGCardType, MTGDeckMainCard, MTGDeckMaybeCard, MTGDeckSideCard,
                     MTGFormat, MTGLegalityExc, MTGOwnedCard, MTGRarity)


class MTGCardViewSet(viewsets.ModelViewSet):
    model = MTGCard


class MTGExpansionViewSet(viewsets.ModelViewSet):
    model = MTGExpansion


class MTGDeckViewSet(viewsets.ModelViewSet):
    model = MTGDeck


class MTGCardBaseViewSet(viewsets.ModelViewSet):
    model = MTGCardBase


class MTGArchetypeViewSet(viewsets.ModelViewSet):
    model = MTGArchetype


class MTGBlockViewSet(viewsets.ModelViewSet):
    model = MTGBlock


class MTGCardSubtypeViewSet(viewsets.ModelViewSet):
    model = MTGCardSubtype


class MTGCardTypeViewSet(viewsets.ModelViewSet):
    model = MTGCardType


class MTGDeckMainCardViewSet(viewsets.ModelViewSet):
    model = MTGDeckMainCard


class MTGDeckMaybeCardViewSet(viewsets.ModelViewSet):
    model = MTGDeckMaybeCard


class MTGDeckSideCardViewSet(viewsets.ModelViewSet):
    model = MTGDeckSideCard


class MTGFormatViewSet(viewsets.ModelViewSet):
    model = MTGFormat


class MTGLegalityExcViewSet(viewsets.ModelViewSet):
    model = MTGLegalityExc


class MTGOwnedCardViewSet(viewsets.ModelViewSet):
    model = MTGOwnedCard


class MTGRarityViewSet(viewsets.ModelViewSet):
    model = MTGRarity



