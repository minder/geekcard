# coding: utf-8
# from __future__ import unicode_literals
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.fields import ShortUUIDField
from account.models import Player
from django.contrib.auth import get_user_model
import django_select2.models
import django_select2.fields
from taggit.managers import TaggableManager
import os
from django.utils.text import slugify
from django.utils.timezone import now


class MTGBlock(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=200)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


class MTGExpansion(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=255)
    abbrev = models.CharField(_('abbrev'), max_length=4)
    symbol = models.ImageField(_('Symbol'), upload_to="mtg/exp", blank=True)  # nie do końca potrzebne
    release_date = models.DateField(_('release date'), blank=True, null=True)
    block = models.ForeignKey(MTGBlock, null=True, blank=True, verbose_name=_('block'))
    cards_total = models.PositiveSmallIntegerField(_('total cards'), default=0)  # ile kart wydrukowano w tej edycji (oficjalna wartość)
    is_core = models.BooleanField(_('core'), default=False)
    is_special = models.BooleanField(_('special'), default=False,
                                     help_text=_('Is it something like "Duel Decks" or "Modern Masters"?'))

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-release_date', 'name']


class MTGRarity(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=25)
    color = models.CharField(_('Color'), max_length=7)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


# MTG_RARITY = (
#     (-2, _('Token')),
#     (-1, _('Proxy')),
#     (0, _('Land')),
#     (1, _('Common')),
#     (2, _('Uncommon')),
#     (3, _('Rare')),
#     (4, _('Mythic Rare')),
#     (5, _('Special')),
#     (6, _('Promo')),
# )


class MTGCardType(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=20)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


class MTGCardSubtype(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=20)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


class MTGCardBase(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=250)
    oracle_rules = models.TextField(_('rules'), blank=True)
    power = models.CharField(_('power'), max_length=5, blank=True)
    toughness = models.CharField(_('toughness'), max_length=5, blank=True)
    loyalty = models.CharField(_('loyalty'), max_length=5, blank=True)
    card_type = models.ManyToManyField(MTGCardType, blank=True, null=True, verbose_name=_('card type'))
    card_subtype = models.ManyToManyField(MTGCardSubtype, blank=True, null=True, verbose_name=_('card subtype'))
    mana_cost = models.CharField(_('mana cost'), max_length=30, blank=True)
    cmc = models.PositiveSmallIntegerField(_('converted mana cost'), default=0)

    def mana_cost_as_list(self):
        """
        Returns the mana cost converted to sequence of images.
        """
        result = []
        mc = self.mana_cost
        mc_split = mc.split('(')
        mc = mc_split[0]
        for char in mc:
            if char in "0123456789GBWURXYZ":
                result.append(char.lower())
        if len(mc_split) > 1:
            for cost in mc_split[1:]:
                cost = cost.replace('/', '').replace(')', '')
                result.append(cost.lower())
        return result

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


class Artist(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=250)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


class Language(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=20)
    abbrev = models.CharField(_('abbrev'), max_length=5)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


def mtg_upload_to(instance, filename):
    try:
        ext = filename.split('.')[-1].lower()
    except IndexError:
        ext = 'jpg'
    return os.path.join('mtg', 'cards', instance.language.abbrev.lower(), instance.expansion.abbrev.lower(),
                        slugify(instance.base.name) + '.' + ext)


class MTGCard(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('Name'), max_length=250, blank=True)  # nazwa w danej wersji językowej
    language = models.ForeignKey(Language, verbose_name=_('Language'), blank=True, null=True)
    base = models.ForeignKey(MTGCardBase, verbose_name=_("Card's Base"))
    # artwork = models.ImageField(_('Artwork'), upload_to=mtg_upload_to, blank=True)
    artwork = models.URLField(_('Artwork'), blank=True)  # will use Flickr to store images TODO: make special FlickrField :)
    artist = models.ForeignKey(Artist, verbose_name=_('Artist'), blank=True, null=True)
    rarity = models.ForeignKey(MTGRarity, verbose_name=_('Rarity'))
    expansion = models.ForeignKey(MTGExpansion, verbose_name=_('Expansion'))
    flavor_text = models.TextField(_('Flavor text'))
    number = models.PositiveSmallIntegerField(_('Number'), default=0)  # numer karty w edycji
    gatherer_id = models.PositiveIntegerField(_('Gatherer ID'), default=0)
    version = models.CharField(_('version'), max_length=60, blank=True,
                               help_text=_('Sometimes card with same number in set appears in several versions (Zendikar lands, fullart or release promos)')
    )

    def __unicode__(self):
        return "%s %s" % (self.expansion.abbrev, self.base.name)

    class Meta:
        ordering = ['name', ]

    def get_reprints(self):
        return self.base.mtgcard_set.all()


CONDITION = (
    (0, _('Mint/Near Mint')),
    (1, _('Slightly Played')),
    (2, _('Moderately Played')),
    (3, _('Heavily Played')),
)


class MTGOwnedCard(models.Model):
    uuid = ShortUUIDField()
    card = models.ForeignKey(MTGCard, verbose_name=_('Card'))
    owner = models.ForeignKey(Player, verbose_name=_('Owner'))
    condition = models.PositiveSmallIntegerField(_('Condition'), choices=CONDITION, default=0)
    foil = models.BooleanField(_('Foil'), default=False)
    signed = models.BooleanField(_('Signed'), default=False)
    # date = models.DateTimeField(_('Date'), default=now, help_text=_('The date this card has been acquired.'))
    comment = models.TextField(_('Comment'), blank=True)
    hand_polished = models.BooleanField(_('polished by hand'), default=False,
                                        help_text=_('Was the card modified by hand? If yes, warn before auto-deleting in front-end.'))

    tags = TaggableManager()

    def __unicode__(self):
        return "%s owned by %s" % (self.card, self.owner)


class MTGArchetype(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


class MTGDeckMainCard(models.Model):
    uuid = ShortUUIDField()
    deck = models.ForeignKey('MTGDeck', verbose_name=_('deck'))
    card = models.ForeignKey(MTGCardBase, verbose_name=_('base card'))
    exp = models.ForeignKey(MTGExpansion, blank=True, null=True, verbose_name=_('expansion'))
    quantity = models.PositiveSmallIntegerField(_('Quantity'))


class MTGDeckSideCard(models.Model):
    uuid = ShortUUIDField()
    deck = models.ForeignKey('MTGDeck', verbose_name=_('deck'))
    card = models.ForeignKey(MTGCardBase, verbose_name=_('base card'))
    exp = models.ForeignKey(MTGExpansion, blank=True, null=True, verbose_name=_('expansion'))
    quantity = models.PositiveSmallIntegerField(_('Quantity'))


class MTGDeckMaybeCard(models.Model):
    uuid = ShortUUIDField()
    deck = models.ForeignKey('MTGDeck', verbose_name=_('deck'))
    card = models.ForeignKey(MTGCardBase, verbose_name=_('base card'))
    exp = models.ForeignKey(MTGExpansion, blank=True, null=True, verbose_name=_('expansion'))
    quantity = models.PositiveSmallIntegerField(_('Quantity'))


MTG_ROTATION = (
    (0, _('No')),
    (1, _('Standard')),
)


class MTGFormat(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('name'), max_length=15)
    badge_color = models.CharField(_('badge color'), max_length=7, blank=True)
    abbrev = models.CharField(_('abbrev'), max_length=7)
    superset = models.ForeignKey('MTGFormat', verbose_name=_('superset'), related_name='subsets',
                                 blank=True, null=True, help_text=_('Indicate the next superset.'))
    rotation = models.PositiveSmallIntegerField(_('rotation'), default=0, choices=MTG_ROTATION)

    def __unicode__(self):
        return self.name


class MTGDeck(models.Model):
    uuid = ShortUUIDField()
    name = models.CharField(_('name'), max_length=255)
    archetypes = models.ManyToManyField(MTGArchetype, verbose_name=_('archetypes'))
    mainboard = models.ManyToManyField(MTGCardBase, through=MTGDeckMainCard,
                                       related_name='in_mainboard', verbose_name=_('mainboard'))
    sideboard = models.ManyToManyField(MTGCardBase, through=MTGDeckSideCard,
                                       related_name='in_sideboard', verbose_name=_('sideboard'))
    maybeboard = models.ManyToManyField(MTGCardBase, through=MTGDeckMaybeCard,
                                        related_name='in_maybeboard', verbose_name=_('maybeboard'))
    in_progress = models.BooleanField(_('Work in progress'), default=False)
    format = models.ForeignKey(MTGFormat, null=True, blank=True, verbose_name=_('format'))

    tags = TaggableManager()

    # umożliwić wprowadzanie decklisty z palca! -> w formularzu

    def __unicode__(self):
        return self.name


LEGALITIES = (
    (0, _('Legal')),
    (1, _('Restricted')),
    (2, _('Banned')),
)


class MTGLegalityExc(models.Model):
    """
    Wyjątek od legalności danej karty w danym formacie.
    Domyślnie, jeśli brakuje wpisu, każda karta powinna być traktowana jako legalna wg odpowiedniego schematu.
    Każdy z formatów ma czasowo okresloną legalność. Normalne dodatki - dwa lata, core sety - rok.
    """
    uuid = ShortUUIDField()
    card = models.ForeignKey(MTGCardBase, verbose_name=_('card'))
    format = models.ForeignKey(MTGFormat, verbose_name=_('format'))
    legality = models.PositiveSmallIntegerField(_('legality'), choices=LEGALITIES)

    def __unicode__(self):
        return "%(card)s is %(legality)s in %(format)s" % {'card': self.card, 'format': self.format, 'legality': self.legality}
