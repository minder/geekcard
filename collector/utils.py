# coding: utf-8

# import flickr_api
# from PIL import Image
# from cStringIO import StringIO
# from django.conf import settings
# flickr_api.set_keys(api_key=settings.FLICKR_API['KEY'], api_secret=settings.FLICKR_API['SECRET'])
# flickr_api.set_auth_handler('./auth_handler.flickr')
# img = Image.new('RGB', (100, 100))
# plik = StringIO()
# img.save(plik, 'JPEG')
# plik.seek(0)


def get_photo_url(photo):
    info = photo.getInfo()
    return "https://farm{farm}.staticflickr.com/{server}/{id}_{secret}.{originalformat}".format(**info)

