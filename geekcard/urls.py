#coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .api import router


admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^avatar/', include('avatar.urls')),
    url(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/v1/', include(router.urls)),
    #url(r'^account/$', 'account.views.show_my_profile'),  # redirect do własnego profilu? nieee...
    url(r'^account/', include('registration.backends.default.urls')),
    # player/ -> lista graczy
    # player/[login]/ -> profil gracza
    # league/ -> strona ze zbiorczymi wynikami lig, dany gracz podświetlony
    url(r'^league/', include('league.urls.league', namespace='league')),
    url(r'^league-utils/', include('league.urls.utils', namespace='league_utils')),
    url(r'^player/', include('league.urls.player', namespace='player')),
    url(r'^rental/', include('lending.urls', namespace='lending')),
    # league/[game]/ -> konkretna liga (gra)
    # league/[game]/[slug]/ -> sezon w danej grze
    # url(r'^select2/', include('select2.urls')),
    url(r'^select2/', include('django_select2.urls')),
    # url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^collector/', include('collector.urls', namespace='collector')),
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

