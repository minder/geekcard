#coding: utf-8
"""
Django settings for geekcard project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
from __future__ import unicode_literals

from configurations import Configuration
import os
import sys
import hashlib
import uuid

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/


def get_secret_key(base_dir='.'):
    def gen_key(key_path):
        with open(key_path, 'w') as key_file:
            key = hashlib.sha512(str(uuid.uuid4()).encode('utf8')).hexdigest()
            key_file.write(key)
        return key

    path = os.path.join(base_dir, '.secret.key')

    try:
        secret_key = open(path).read()
        assert secret_key, "Wrong secret key"
    except (IOError, AssertionError):
        secret_key = gen_key(path)
    return secret_key


class Production(Configuration):
    SECRET_KEY = get_secret_key(BASE_DIR)
    TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

    # Application definition

    INSTALLED_APPS = (
        # 'django_admin_bootstrapped.bootstrap3',
        # 'django_admin_bootstrapped',
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        ###### przydatne biblioteki
        'south',
        'avatar',
        'rest_framework',
        'registration',
        'bootstrap3',
        'fontawesome',
        'django_select2',
        # 'select2',
        'taggit',
        'django_extensions',
        ###### aplikacje własne
        'account',
        'league',
        'httpbl',
        'lending',
        'collector',
        # 'rest_framework_swagger',

    )

    MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        'httpbl.middleware.HttpBLMiddleware',

    )

    ROOT_URLCONF = 'geekcard.urls'

    WSGI_APPLICATION = 'geekcard.wsgi.application'

    # Internationalization
    # https://docs.djangoproject.com/en/1.6/topics/i18n/

    LANGUAGE_CODE = 'pl'

    TIME_ZONE = 'Europe/Warsaw'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    AUTH_USER_MODEL = 'account.Player'
    LOGIN_REDIRECT_URL = 'player:profile'
    LOGIN_URL = 'auth_login'
    LOGOUT_URL = 'auth_logout'

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/1.6/howto/static-files/

    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR, 'public/static')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'public/media')
    MEDIA_URL = '/media/'

    REST_FRAMEWORK = {
        'DEFAULT_PERMISSION_CLASSES': [
            'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
        ]

    }

    ACCOUNT_ACTIVATION_DAYS = 7

    REGISTRATION_OPEN = False

    # django_select2

    AUTO_RENDER_SELECT2_STATICS = False
    SELECT2_BOOTSTRAP = True

    SOUTH_MIGRATION_MODULES = {
        'taggit': 'taggit.south_migrations',
    }

    TEMPLATE_CONTEXT_PROCESSORS = (
        "django.contrib.auth.context_processors.auth",
        "django.core.context_processors.request",
        "django.core.context_processors.debug",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
    )

    # local settings imported at end to correctly provide overrides of above settings


class Dev(Production):
    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True

    TEMPLATE_DEBUG = True

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'db.sqlite3',
        }
    }

    HTTPBL_KEY = os.environ.setdefault('HTTPBL_KEY', "")

