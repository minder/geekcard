# coding: utf-8
__author__ = 'minder'

from rest_framework import routers
from account.api import UserViewSet, ClubCardViewSet
from league.api import (GameViewSet, GameIDViewSet, LeagueSeasonViewSet, EventCategoryViewSet,
                        MatchViewSet, MatchResultViewSet, RewardViewSet)
from lending.api import (LendGameSet, LendItemSet)

from collector.api import (MTGArchetypeViewSet, MTGBlockViewSet, MTGCardBaseViewSet,
                           MTGCardSubtypeViewSet, MTGCardTypeViewSet, MTGCardViewSet, MTGDeckMainCardViewSet,
                           MTGDeckMaybeCardViewSet, MTGDeckSideCardViewSet, MTGDeckViewSet, MTGExpansionViewSet,
                           MTGFormatViewSet, MTGLegalityExcViewSet, MTGOwnedCardViewSet, MTGRarityViewSet)

router = routers.DefaultRouter()

router.register(r'user', UserViewSet)
router.register(r'clubcard', ClubCardViewSet)
router.register(r'game', GameViewSet)
router.register(r'gameid', GameIDViewSet)
router.register(r'league_season', LeagueSeasonViewSet)
router.register(r'event_category', EventCategoryViewSet)
router.register(r'match', MatchViewSet)
router.register(r'match_result', MatchResultViewSet)
router.register(r'reward', RewardViewSet)
router.register(r'lend_game', LendGameSet)
router.register(r'lend_item', LendItemSet)
router.register(r'mtg_archetype', MTGArchetypeViewSet)
router.register(r'mtg_block', MTGBlockViewSet)
router.register(r'mtg_card_base', MTGCardBaseViewSet)
router.register(r'mtg_card_subtype', MTGCardSubtypeViewSet)
router.register(r'mtg_card_type', MTGCardTypeViewSet)
router.register(r'mtg_card', MTGCardViewSet)
router.register(r'mtg_deck_main', MTGDeckMainCardViewSet)
router.register(r'mtg_deck_maybe', MTGDeckMaybeCardViewSet)
router.register(r'mtg_deck_side', MTGDeckSideCardViewSet)
router.register(r'mtg_deck', MTGDeckViewSet)
router.register(r'mtg_expansion', MTGExpansionViewSet)
router.register(r'mtg_format', MTGFormatViewSet)
router.register(r'mtg_legality', MTGLegalityExcViewSet)
router.register(r'mtg_owned_card', MTGOwnedCardViewSet)
router.register(r'mtg_rarity', MTGRarityViewSet)
